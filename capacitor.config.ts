import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.lagom.ht',
  appName: 'Lagom',
  webDir: 'www',
  server: {
    cleartext: true,
    allowNavigation: [
      "http://*/*",
      "https://*/*"
    ]
  },
  ios: {
    scheme: 'Lagom'
  },
  bundledWebRuntime: false,
  plugins: {
    PushNotifications: {
      presentationOptions: ["badge", "sound", "alert"]
    },
    LocalNotifications: {
      smallIcon: "ic_launcher",
      iconColor: "#283357",
      sound: "beep.wav"
    },
    CapacitorHttp: {
      enabled: true
    },
    Keyboard:{
      resizeOnFullScreen: true
    }
  }
};

export default config;
