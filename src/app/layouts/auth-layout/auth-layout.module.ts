import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthLayoutPageRoutingModule } from './auth-layout-routing.module';
import { AuthLayoutPage } from './auth-layout.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    AuthLayoutPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthLayoutPageRoutingModule,
    SharedModule
  ],
})
export class AuthLayoutPageModule {}
