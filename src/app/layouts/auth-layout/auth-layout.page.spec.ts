import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { AuthLayoutPage } from './auth-layout.page';

describe('AuthLayoutPage', () => {
  let component: AuthLayoutPage;
  let fixture: ComponentFixture<AuthLayoutPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AuthLayoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
