import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PATHS } from 'src/app/core/config/constants/app.constants';
import { AuthLayoutPage } from './auth-layout.page';
import { httpInterceptorProviders } from 'src/app/core/interceptors';

const routes: Routes = [
  {
    path: '',
    component: AuthLayoutPage,
    children: [
      {
        path: PATHS.LOGIN,
        loadChildren: () => import('../../pages/auth/login/login.module').then(m => m.LoginPageModule)
      },
      {
        path: PATHS.LOGIN_FORM,
        loadChildren: () => import('../../pages/auth/login-form/login-form.module').then(m => m.LoginFormPageModule)
      },
      {
        path: PATHS.FORGOT_PASSWORD,
        loadChildren: () => import('../../pages/auth/forgot-password/forgot-password.module').then(m => m.ForgotPasswordPageModule)
      },
      {
        path: `${PATHS.CODE_PASSWORD}/:email/:code`,
        loadChildren: () => import('../../pages/auth/code-password/code-password.module').then(m => m.CodePasswordPageModule)
      },
      {
        path: `${PATHS.FORM_PASSWORD}/:email/:code`,
        loadChildren: () => import('../../pages/auth/form-password/form-password.module').then(m => m.FormPasswordPageModule)
      },
      {
        path: PATHS.REGISTER_PARENT,
        loadChildren: () => import('../../pages/auth/register-parent/register-parent.module').then(m => m.RegisterParentPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthLayoutPageRoutingModule {}
