import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ParentLayoutPage } from './parent-layout.page';
import { httpInterceptorProviders } from 'src/app/core/interceptors';

const routes: Routes = [
  {
    path: '',
    component: ParentLayoutPage,
    children: [
      {
        path: PATHS.HOME_PARENT,
        loadChildren: () => import('../../pages/parent/home-parent/home-parent.module').then(m => m.HomeParentPageModule)
      },
      {
        path: `${PATHS.FORM_CHILD_PARENT}/:header/:edit`,
        loadChildren: () => import('../../pages/parent/form-child/form-child.module').then(m => m.FormChildPageModule)
      },
      {
        path: PATHS.PROFILE_PARENT,
        loadChildren: () => import('../../pages/parent/profile/profile.module').then(m => m.ProfilePageModule)
      },
      {
        path: `${PATHS.PROFILE_PARENT}/:edit`,
        loadChildren: () => import('../../pages/parent/profile/form-edit/form-edit.module').then(m => m.FormEditPageModule)
      },
      {
        path: PATHS.NOTIFICATION_PARENT,
        loadChildren: () => import('../../pages/parent/notifications/notifications.module').then(m => m.NotificationsPageModule)
      },
      {
        path: PATHS.CONFIGURATION_PARENT,
        loadChildren: () => import('../../pages/parent/notifications/configuration/configuration.module').then(m => m.ConfigurationPageModule)
      },
      {
        path: PATHS.FAQ_PARENT,
        loadChildren: () => import('../../pages/parent/faq/faq.module').then(m => m.FaqPageModule)
      },
      {
        path: PATHS.ROOT_CHILDREN,
        loadChildren: () => import('../../layouts/children-navigation/children-navigation.module').then(m => m.ChildrenNavigationPageModule)
      },
      {
        path: `${PATHS.ROOT_CHILDREN}/${PATHS.BALANCE_CHILDREN}/${PATHS.BALANCE_RECHARGE_CHILDREN}`,
        loadChildren: () => import('../../pages/children/balance/recharge/recharge.module').then(m => m.RechargePageModule)
      },
      {
        path: `${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}/${PATHS.MENU_MONTHLY_CHILDREN}`,
        loadChildren: () => import('../../pages/children/preorder/menu-monthly/menu-monthly.module').then(m => m.MenuMonthlyPageModule)
      },
      {
        path: `${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}/${PATHS.MENU_DIARY_CHILDREN}`,
        loadChildren: () => import('../../pages/children/preorder/menu-diary/menu-diary.module').then(m => m.MenuDiaryPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    httpInterceptorProviders,
  ],
})
export class ParentLayoutPageRoutingModule {}
