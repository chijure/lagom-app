import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ModalService } from '../../shared/services/modal/modal.service';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';

@Component({
  selector: 'app-parent-layout',
  templateUrl: './parent-layout.page.html',
  styleUrls: ['./parent-layout.page.scss'],
})
export class ParentLayoutPage implements OnInit {
  public menuItems = [
    {
      title: 'Mi perfil',
      path: `/${PATHS.ROOT_PARENT}/${PATHS.PROFILE_PARENT}`,
      isLink: false,
      isBadge: 0,
    },
    {
      title: 'Mis hijos',
      path: `/${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`,
      isLink: false,
      isBadge: 0,
    },
    {
      title: 'Notificaciones',
      path: `/${PATHS.ROOT_PARENT}/${PATHS.NOTIFICATION_PARENT}`,
      isLink: false,
      isBadge: 0,
    },
    {
      title: 'Ayuda y soporte',
      path: `/${PATHS.ROOT_PARENT}/${PATHS.FAQ_PARENT}`,
      isLink: false,
      isBadge: 0,
    },
    /* { title: 'Encuesta satifacción', path: '', isLink: true  }, */
  ];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private modalService: ModalService,
    private navController: NavController
  ) {}

  ngOnInit() {
    this.store.select('service', 'notification').subscribe({
      next: (res: any) => {
        this.menuItems[2].isBadge = res;
      },
    });
  }

  privacy() {
    window.open('https://www.lagom.pe/web/pdf/Confidenciality_Statement.pdf', '_blank');
  }

  logout() {
    this.modalService.showConfirmation(
      'Cerrar sesión',
      '¿Está seguro que desea cerrar sesión?',
      'Si',
      'No',
      () => {
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        localStorage.removeItem('appDefault');
        this.navController.navigateRoot(['/']);
      },
      () => {}
    );
  }
}
