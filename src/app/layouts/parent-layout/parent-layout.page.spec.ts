import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { ParentLayoutPage } from './parent-layout.page';

describe('ParentLayoutPage', () => {
  let component: ParentLayoutPage;
  let fixture: ComponentFixture<ParentLayoutPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ParentLayoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
