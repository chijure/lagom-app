import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentLayoutPageRoutingModule } from './parent-layout-routing.module';
import { ParentLayoutPage } from './parent-layout.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ParentLayoutPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentLayoutPageRoutingModule,
    SharedModule
  ],
})
export class ParentLayoutPageModule {}
