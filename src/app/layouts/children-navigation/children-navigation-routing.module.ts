import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ChildrenNavigationPage } from './children-navigation.page';
import { httpInterceptorProviders } from 'src/app/core/interceptors';

const routes: Routes = [
  {
    path: '',
    component: ChildrenNavigationPage,
    children: [
      {
        path: PATHS.HOME_CHILDREN,
        loadChildren: () => import('../../pages/children/preorder/preorder.module').then(m => m.PreorderPageModule)
      },
      {
        path: PATHS.BALANCE_CHILDREN,
        loadChildren: () => import('../../pages/children/balance/balance.module').then(m => m.BalancePageModule)
      },
      {
        path: PATHS.CONSUMPTION_CHILDREN,
        loadChildren: () => import('../../pages/children/consumption/consumption.module').then(m => m.ConsumptionPageModule)
      },
      {
        path: PATHS.PROFILE_CHILDREN,
        loadChildren: () => import('../../pages/children/profile/profile.module').then(m => m.ProfilePageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChildrenNavigationPageRoutingModule {}
