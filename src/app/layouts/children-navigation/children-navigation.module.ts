import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChildrenNavigationPageRoutingModule } from './children-navigation-routing.module';
import { ChildrenNavigationPage } from './children-navigation.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ChildrenNavigationPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChildrenNavigationPageRoutingModule,
    SharedModule
  ],
})
export class ChildrenNavigationPageModule {}
