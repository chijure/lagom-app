import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { ChildrenNavigationPage } from './children-navigation.page';

describe('ChildrenNavigationPage', () => {
  let component: ChildrenNavigationPage;
  let fixture: ComponentFixture<ChildrenNavigationPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ChildrenNavigationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
