import { Component, OnInit } from '@angular/core';
import { PATHS } from '../../core/config/constants/app.constants';

@Component({
  selector: 'app-children-navigation',
  templateUrl: './children-navigation.page.html',
  styleUrls: ['./children-navigation.page.scss'],
})
export class ChildrenNavigationPage implements OnInit {

  public routes = PATHS;

  constructor() { }

  ngOnInit() {
  }

}
