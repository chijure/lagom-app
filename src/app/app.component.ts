import { Component, NgZone, OnInit, Optional } from '@angular/core';
import { addIcons } from 'ionicons';
import { register } from 'swiper/element/bundle';
import { IonRouterOutlet, NavController, Platform } from '@ionic/angular';
import { FcmService } from './shared/services/fcm/fcm.service';
import { App } from '@capacitor/app';
import { ModalService } from './shared/services/modal/modal.service';
import { Router } from '@angular/router';
import { PATHS } from './core/config/constants/app.constants';
import { AuthService } from './core/services/auth/auth.service';
import { VersionService } from './core/services/version/version.service';
import { environment } from 'src/environments/environment';
import { AppUpdate } from '@capawesome/capacitor-app-update';
import { Capacitor } from '@capacitor/core';

register();

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private fcm: FcmService,
    private modalService: ModalService,
    private router: Router,
    private navController: NavController,
    private authService: AuthService,
    private ngZone: NgZone,
    private versionService: VersionService,
    @Optional() private routerOutlet?: IonRouterOutlet
  ) {
    addIcons({
      'eye-theme': 'assets/icon/svg/eye-theme.svg',
      'eye-off-theme': 'assets/icon/svg/eye-off-theme.svg',
      'arrow-left-theme': 'assets/icon/svg/arrow-left-theme.svg',
      'arrow-right-theme': 'assets/icon/svg/arrow-right-theme.svg',
      'pen-theme': 'assets/icon/svg/pen-theme.svg',
      'config-theme': 'assets/icon/svg/config-theme.svg',
      'message-theme': 'assets/icon/svg/message-theme.svg',
      'box-plus-theme': 'assets/icon/svg/box-plus-theme.svg',
      'call-theme': 'assets/icon/svg/call-theme.svg',
      'pre-order-theme': 'assets/icon/svg/pre-order-theme.svg',
      'balance-theme': 'assets/icon/svg/balance-theme.svg',
      'historial-theme': 'assets/icon/svg/historial-theme.svg',
      'user-theme': 'assets/icon/svg/user-theme.svg',
      'file-theme': 'assets/icon/svg/file-theme.svg',
      'delete-theme': 'assets/icon/svg/delete-theme.svg',
      'calendar-theme': 'assets/icon/svg/calendar-theme.svg',
      'download-theme': 'assets/icon/svg/download-theme.svg',
    });

    App.addListener('backButton', ({ canGoBack }) => {
      if (!canGoBack) {
        this.exitAlertConfirm();
      } else {
        //window.history.back();
        const notAllowedRoutes = [
          '/',
          `/${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.PROFILE_PARENT}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.NOTIFICATION_PARENT}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.FAQ_PARENT}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.FORM_CHILD_PARENT}/0/0`,
          `/${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.BALANCE_CHILDREN}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.CONSUMPTION_CHILDREN}`,
          `/${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.PROFILE_CHILDREN}`,
        ];

        console.log('entro0', this.router.url);
        if (notAllowedRoutes.includes(this.router.url)) {
          console.log('entro1');
          this.exitAlertConfirm();
          //window.history.back();
        } else {
          console.log('entro2');
          window.history.back();
        }
      }
    });

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.fcm.initPush();

      this.validateVersion();

      if (this.authService.isLoggedIn()) {
        this.navController.navigateRoot([`${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`]);
      } else {
        this.navController.navigateRoot([`/`]);
      }
    });
  }

  exitAlertConfirm() {
    this.modalService.showConfirmation(
      'Cerrar la aplicación',
      '¿Seguro que quieres cerrar la aplicación?',
      'Confirmar',
      'No',
      () => {
        //navigator['app'].exitApp();
        App.exitApp();
      },
      () => { }
    );
  }

  validateVersion(){
    if (!this.platform.is('capacitor')) {
      return;
    }
    AppUpdate.getAppUpdateInfo().then((appUpdateInfo:any) => {
      const currentVersionName = appUpdateInfo.currentVersionName;
      const CFBundleShortVersionString = appUpdateInfo.CFBundleShortVersionString;

      console.log('appUpdateInfo', appUpdateInfo);
      console.log('currentVersionName', currentVersionName);
      console.log('CFBundleShortVersionString', CFBundleShortVersionString);

      const platform = this.platform.is('ios') ? 'ios' : 'android';
      const currentVersion = this.platform.is('ios') ? CFBundleShortVersionString : currentVersionName;
      this.versionService.getVersion(platform).subscribe({
        next: async (res : any) => {
          if (String(res.version) !== String(currentVersion)) {
            this.modalService.showInfo('Nueva versión', 'Tenemos una nueva versión disponible para ti; por favor, descárgala.', 'OK', () => {
              if(res.isRequired){
                this.openAppStore();
                /* if(platform === 'android'){
                  this.performImmediateUpdate();
                } else {
                  this.openAppStore();
                } */
              }
            })
          }
        }
      })

    });
  }

  public async openAppStore(): Promise<void> {
    await AppUpdate.openAppStore();
  }

  public async performImmediateUpdate(): Promise<void> {
    await AppUpdate.performImmediateUpdate();
  }
}
