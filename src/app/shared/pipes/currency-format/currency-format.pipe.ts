import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyFormat'
})
export class CurrencyFormatPipe implements PipeTransform {

  transform(value: number, currencyCode: string = 'PEN'): string {
    const currencyFormat = new Intl.NumberFormat('es-PE', { style: 'currency', currency: currencyCode });
    return currencyFormat.format(value || 0);
  }

}
