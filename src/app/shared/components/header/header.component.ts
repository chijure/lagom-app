import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { TypeHeader } from 'src/app/core/config/constants/app.constants';
import { ICONS } from 'src/app/core/config/constants/icon.constants';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { ObservableService } from '../../services/observable/observable.service';
import { ServiceActions } from 'src/app/core/redux/actions/service.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() type: TypeHeader = 'title';
  @Input() title: string = '';
  @Input() titleAlign: string = 'left';
  @Output() actionIcon?: any = new EventEmitter<any>();

  public icon = ICONS;
  public childList: any = {};
  public child: any = {};

  constructor(
    public actionSheetController: ActionSheetController,
    private store: Store<AppState>,
    private observableService: ObservableService
  ) {}

  ngOnInit() {
    this.store.select('child').subscribe({
      next: (res: any) => {
        this.child = res.child;
        this.childList = res.childList;
      },
    });
  }

  async presentActionSheet() {
    const buttons = this.childList.map((child: any) => ({
      text: `${child.firstName}`,
      handler: () => {
        this.child = child;
        this.store.dispatch(ChildActions.setChild({ child: this.child}));
        this.store.dispatch(ServiceActions.setServiceList({serviceList: []}))
        this.observableService.changeChildSubject$.next(this.child);
      },
    }));

    buttons.push({
      text: 'Cancelar',
      role: 'cancel',
      handler: () => {
        console.log('Cancelar seleccionado');
      },
    });

    const actionSheet = await this.actionSheetController.create({
      header: 'Seleccione un hijo',
      buttons: buttons,
    });

    await actionSheet.present();
  }

  onActionIcon() {
    this.actionIcon.next(true);
  }
}
