import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateSelectService {

  getDates(): any[] {
    const currentDate = new Date();
    const formattedCurrentDate = this.formatDate(currentDate);
    const startOfCurrentWeek = this.calculateStartOfCurrentWeek(currentDate);
    const startOfLastWeek = this.calculateStartOfLastWeek(currentDate);

    return [
      { value: formattedCurrentDate, label: 'Hoy', slug: 'days-range' },
      { value: startOfCurrentWeek, label: 'Semana actual', slug: 'week-range' },
      { value: startOfLastWeek, label: 'Semana anterior', slug: 'week-range' },
      { value: this.calculateStartOfNextWeek(currentDate), label: 'Próxima semana', slug: 'week-range' },
      { value: this.formatCurrentMonth(currentDate), label: 'Mes actual', slug: 'month-range' },
    ];
  }

  getDatesNotifications(): any[] {
    const currentDate = new Date();
    const formattedCurrentDate = this.formatDate(currentDate);

    return [
      { value: formattedCurrentDate, label: 'Hoy', slug: 'days-range' },
      { value: this.calculateStartOfCurrentWeek(currentDate), label: 'Semana actual', slug: 'week-range' },
      { value: this.formatCurrentMonth(currentDate), label: 'Mes actual', slug: 'month-range' },
    ];
  }

  private formatDate(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear().toString();
    return `${year}-${month}-${day}`;
  }

  private calculateStartOfCurrentWeek(date: Date): string {
    const dayOfWeek = date.getDay() || 7;
    date.setDate(date.getDate() - dayOfWeek + 1);
    const startOfWeek = new Date(date);
    return `${this.formatDate(startOfWeek)}`;
  }

  private calculateStartOfNextWeek(date: Date): string {
    const nextWeek = new Date(date);
    nextWeek.setDate(nextWeek.getDate() + (7 - nextWeek.getDay()) + 1);
    return `${this.formatDate(nextWeek)}`;
  }

  private formatCurrentMonth(date: Date): string {
    const month = moment().format('M').padStart(2, '0');
    const year = moment().format('YYYY').toString();
    return `${year}-${month}`;
  }

  private formatNextMonth(date: Date): string {
    const nextMonth = new Date(date);
    nextMonth.setMonth(nextMonth.getMonth() + 1);
    const month = (nextMonth.getMonth() + 1).toString().padStart(2, '0');
    const year = nextMonth.getFullYear().toString();
    return `${year}-${month}`;
  }

  private calculateStartOfLastWeek(date: Date): string {
    const lastWeek = new Date(date);
    lastWeek.setDate(lastWeek.getDate() - 7);
    return this.calculateStartOfCurrentWeek(lastWeek);
  }
}
