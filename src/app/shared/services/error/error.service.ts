import { Injectable } from '@angular/core';
import { ModalService } from '../modal/modal.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private modalService: ModalService,
    private navController: NavController
  ) { }

  customError(error: HttpErrorResponse){

    console.log('SERVER ERROR...', error);
    const { status } = error;
    console.log('status', status);
    console.log('typeof', typeof error.error.error);
    console.log('error', error.error.error);
    if(status === 0){
      this.modalService.showInfo('Error', 'Contacte a su administrador error de servidor: '+ error.statusText);
    } else if (status === 400 || status === 404) {
      if (typeof error.error.error  === 'string') {
        this.modalService.showInfo('Error', error.error.error);
      } else if (typeof error.error.error  === 'object') {
        if(error.error.error.hasOwnProperty('message') && Array.isArray(error.error.error.message)){
          const errorMessage = error.error.error.message.join(', ');
          this.modalService.showInfo('Error', `Tienes los siguientes errores: ${errorMessage}`);
        } else if(error.error.error.hasOwnProperty('message') && typeof error.error.error.message  === 'string'){
          this.modalService.showInfo('Error', error.error.error.message);
        }
      }
    } else if (status === 401) {
      if (typeof error.error.error  === 'string') {
        this.modalService.showInfo('Error', error.error.error);
      } else if (typeof error.error.error  === 'object') {
        if(error.error.error.hasOwnProperty('message') && Array.isArray(error.error.error.message)){
          const errorMessage = error.error.error.message.join(', ');
          this.modalService.showInfo('Error', `Tienes los siguientes errores: ${errorMessage}`);
        } else if(error.error.error.hasOwnProperty('message') && typeof error.error.error.message  === 'string'){
          this.modalService.showInfo('Sesión expirada', 'Su sesión ha expirado, ingrese nuevamente', 'OK', () => {
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            localStorage.removeItem('appDefault');
            this.navController.navigateRoot(['/']);
          });
        }
      }
    } else if (status === 500) {
      if (typeof error.error.error  === 'string') {
        this.modalService.showInfo('Error', error.error.error);
      } else if (typeof error.error.error  === 'object') {
        this.modalService.showInfo('Error', `Contacte a su administrador código de error: ${error.error.error.code} - SQL`);
      }
    }
  }

  customErrorSendCode(error: HttpErrorResponse){

    console.log('SERVER ERROR...', error);
    const { status } = error;
    console.log('status', status);
    console.log('typeof', typeof error.error.error);
    console.log('error', error.error.error);
    if(status === 0){
      this.modalService.showInfo('Error', 'Contacte a su administrador error de servidor: '+ error.statusText);
    } else if (status === 400) {
      if (typeof error.error.error  === 'string') {
        this.modalService.showInfo('Error', error.error.error);
      } else if (typeof error.error.error  === 'object') {
        if(error.error.error.hasOwnProperty('message') && Array.isArray(error.error.error.message)){
          const errorMessage = error.error.error.message.join(', ');
          this.modalService.showInfo('Error', `Tienes los siguientes errores: ${errorMessage}`);
        } else if(error.error.error.hasOwnProperty('message') && typeof error.error.error.message  === 'string'){
          this.modalService.showInfo('Error', error.error.error.message);
        }
      }
    } else if(status === 404){
      this.modalService.showInfo('Correo no encontrado', 'El correo no se encuentra registrado en el app de Lagom.');
    } else if (status === 401) {
      if (typeof error.error.error  === 'string') {
        this.modalService.showInfo('Error', error.error.error);
      } else if (typeof error.error.error  === 'object') {
        if(error.error.error.hasOwnProperty('message') && Array.isArray(error.error.error.message)){
          const errorMessage = error.error.error.message.join(', ');
          this.modalService.showInfo('Error', `Tienes los siguientes errores: ${errorMessage}`);
        } else if(error.error.error.hasOwnProperty('message') && typeof error.error.error.message  === 'string'){
          this.modalService.showInfo('Sesión expirada', 'Su sesión ha expirado, ingrese nuevamente', 'OK', () => {
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            localStorage.removeItem('appDefault');
            this.navController.navigateRoot(['/']);
          });
        }
      }
    } else if (status === 500) {
      if (typeof error.error.error  === 'string') {
        this.modalService.showInfo('Error', error.error.error);
      } else if (typeof error.error.error  === 'object') {
        this.modalService.showInfo('Error', `Contacte a su administrador código de error: ${error.error.error.code} - SQL`);
      }
    }
  }

  errorForm(form: FormGroup){
    for (const key of Object.keys(form.controls)) {
      if (form.controls[key].status === 'INVALID') {
        const invalidControl = this.getInputByFormControlName(key);
        if(invalidControl){
          invalidControl.setFocus();
        }
        break;
      }
    }
  }

  private getInputByFormControlName(formControlName: string): HTMLIonInputElement | null {
    const inputElements = document.querySelectorAll(`ion-input[formControlName="${formControlName}"]`);
    return inputElements.length > 0 ? inputElements[0] as HTMLIonInputElement : null;
  }
}
