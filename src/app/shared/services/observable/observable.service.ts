import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObservableService {
  public rechargeSubject$: Subject<boolean> = new Subject<boolean>();
  public changeChildSubject$: Subject<boolean> = new Subject<boolean>();

  constructor() { }
}
