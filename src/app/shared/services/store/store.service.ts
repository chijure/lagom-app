import { Injectable } from '@angular/core';
import { ModalService } from '../modal/modal.service';
import { Router } from '@angular/router';
/* import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { ROLES } from 'src/app/core/config/constants/app.constants'; */

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(
    private router: Router,
    private modalService: ModalService,
    /* private store: Store<AppState>, */
    ) { }

  /* async storeRole(admin: Function, superAdmin: Function, cdr: any | null, cashier: Function = () => {}){
    await this.store
      .select('user', 'profile')
      .subscribe({
        next: (res: any) => {
          if(String(res.role.slug) == ROLES.ADMIN){
            admin(res);
            if(!!cdr){
              cdr.detectChanges();
            }
          } else if(String(res.role.slug) == ROLES.SUPERADMIN){
            superAdmin(res);
            if(!!cdr){
              cdr.detectChanges();
            }
          } else if(String(res.role.slug) == ROLES.CASHIER){
            cashier(res);
            if(!!cdr){
              cdr.detectChanges();
            }
          } else {
            this.modalService.showInfo('Usuario', 'Usuario no tiene asignado un rol en el sistema', () => {
              localStorage.clear();
              this.router.navigate(['/'])
            }, 'Aceptar', true);
          }
        }
      });
  } */
}
