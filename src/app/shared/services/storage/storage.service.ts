import { Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { from, Observable } from 'rxjs';

export const APP_TOKEN = 'app_token';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setStorage(key: string, value: any) {
    localStorage.setItem(key, value);
  }

  getStorage(key: string): any {
    // Preferences.migrate();
    return localStorage.getItem(key);
  }

  removeStorage(key: string) {
    localStorage.removeItem(key);
  }

  clearStorage() {
    localStorage.clear();
  }

  getToken(): Observable<any> {
    return from(this.getStorage(APP_TOKEN));
  }

}
