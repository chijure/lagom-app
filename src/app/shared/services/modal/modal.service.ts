import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private alertController: AlertController) {}

  async showInfo(header:string = '', message:string = '', textOk:string = 'OK', ok: Function = () => {}) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [
        {
          text: textOk,
          role: 'confirm',
          handler: () => {
            ok();
            console.log('Alert confirmed');
          },
        },
      ],
      backdropDismiss: false,
      keyboardClose: true,
      cssClass: 'alert-info'
    });

    await alert.present();
  }

  async showConfirmation(header:string = '', message:string = '', textOk:string = 'OK', textCancel:string = 'Cancelar', ok: Function = () => {}, cancel: Function = () => {}) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [
        {
          text: textCancel,
          role: 'cancel',
          handler: () => {
            cancel();
            console.log('Cancel confirmed');
          },
        },
        {
          text: textOk,
          role: 'confirm',
          handler: () => {
            ok();
            console.log('Alert confirmed');
          },
        },
      ],
      backdropDismiss: false,
      keyboardClose: true,
      cssClass: 'alert-confirm'
    });

    await alert.present();
  }

  async showConfirmationMessage(header:string = '', message:string = '', textOk:string = 'OK', textCancel:string = 'Cancelar', ok: Function = () => {}, cancel: Function = () => {}) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [
        {
          text: textCancel,
          role: 'cancel',
          handler: () => {
            cancel();
            console.log('Cancel confirmed');
          },
        },
        {
          text: textOk,
          role: 'confirm',
          handler: () => {
            ok();
            console.log('Alert confirmed');
          },
        },
      ],
      backdropDismiss: false,
      keyboardClose: true,
      cssClass: 'alert-confirm-message'
    });

    await alert.present();
  }
}
