import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ActionPerformed, PushNotifications, PushNotificationSchema, Token } from '@capacitor/push-notifications';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from '../storage/storage.service';
import { LocalNotifications } from '@capacitor/local-notifications';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { ServiceActions } from 'src/app/core/redux/actions/service.actions';

export const FCM_TOKEN = 'push_notification_token';

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  private _redirect = new BehaviorSubject<any>(null);

  get redirect() {
    return this._redirect.asObservable();
  }

  constructor(
    private storage: StorageService,
    private store: Store<AppState>
  ) { }

  initPush() {
    if (Capacitor.getPlatform() !== 'web') {
      this.registerPush();
      // this.getDeliveredNotifications();
    }
  }

  private async registerPush() {
    try {
      await this.addListeners();
      let permStatus = await PushNotifications.checkPermissions();

      if (permStatus.receive === 'prompt') {
        permStatus = await PushNotifications.requestPermissions();
      }

      if (permStatus.receive !== 'granted') {
        throw new Error('User denied permissions!');
      }

      await PushNotifications.register();
    } catch (e) {
      console.log(e);
    }
  }

  async getDeliveredNotifications() {
    const notificationList = await PushNotifications.getDeliveredNotifications();
    console.log('delivered notifications', notificationList);
  }

  addListeners() {
    PushNotifications.addListener(
      'registration',
      async (token: Token) => {
        console.log('My token: ', token);
        const fcm_token = (token?.value);
        this.storage.setStorage(FCM_TOKEN, JSON.stringify(fcm_token));
        console.log('saved',this.storage.getStorage(FCM_TOKEN));
      }
    );

    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error: ' + JSON.stringify(error));
    });

    PushNotifications.addListener(
      'pushNotificationReceived',
      async (notification: PushNotificationSchema) => {
        console.log('Push received foreground: ' + JSON.stringify(notification));
        this.store.dispatch(ServiceActions.setNotification({notification: 1}));

        const data = notification?.data;
        if (data?.redirect) this._redirect.next(data?.redirect);

        /*const notifs = await LocalNotifications.schedule({
          notifications: [{
            id: notify.id,
            title: notify.title,
            body: notify.body,
            attachments: [data.image],
            //color: "#019DF4",
            smallIcon: "res://ic_launcher.png",
            //data: { uuid: data.uuid, link: data.link },
            //icon: data.image,
            //foreground: true,
          }]
        });

         notifs.on("click").subscribe((resultado: any) => {
          console.log('resultado', resultado);
        });
        console.log('scheduled notifications', notifs);*/


      }
    );

    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      async (notification: ActionPerformed) => {
        const data = notification.notification.data;
        console.log('Action performed: ' + JSON.stringify(notification.notification));
        console.log('push data background: ', data);
        this.store.dispatch(ServiceActions.setNotification({notification: 1}));
        if (data?.redirect) this._redirect.next(data?.redirect);
      }
    );
  }

  async removeFcmToken() {
    try {
      const saved_token = JSON.parse((await this.storage.getStorage(FCM_TOKEN)));
      this.storage.removeStorage(saved_token);
    } catch (e) {
      console.log(e);
      throw (e);
    }

  }
}
