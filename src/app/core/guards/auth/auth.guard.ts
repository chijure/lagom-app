import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChildFn, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { catchError, map, of, take, tap } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ToastService } from 'src/app/shared/services/toast/toast.service';
import { PATHS } from '../../config/constants/app.constants';
/* import { Store } from '@ngrx/store';
import * as AppDefaultActions from '../../redux/actions/app.actions';
 */
export const AuthGuard: CanActivateFn = async (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const router = inject(Router);
  const authService = inject(AuthService);

  if (!authService.isLoggedIn()) {
    return router.createUrlTree(['/']);
  } else {

    //permitido
    /* store
      .select('appDefault', 'schoolList')
      .subscribe({
        next: (res: any) => {
          if (Array.isArray(res) && res.length == 0) {
            schoolService.getAll().subscribe({
              next: (school) => {
                if (Array.isArray(school) && school.length) {
                  const transformedSchool = school.map(x => ({id: x.id, name: x.name }));
                  store.dispatch(AppDefaultActions.setSchoolList({ schoolList: transformedSchool }));
                }
              },
              error: (error: HttpErrorResponse) => {
                toastService.openSnackBar(error.statusText);
              }
            });
          }
        }
      }); */

    return true;
  }
};

export const canActivateChild: CanActivateChildFn = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => AuthGuard(route, state);
