import { Action, createReducer, on } from '@ngrx/store';
import { ChildActions } from '../actions/child.actions';
import { Student } from '../../config/interfaces/child.interfaces';

export interface ChildState {
  childList: Student[];
  child: Student;
  loadDataChange: boolean;
}

export const initialState: ChildState = {
  childList: [],
  child: {
    firstName: "",
    lastName: "",
    typeDocument: "",
    documentNumber: "",
    studentCode: "",
    medicalConditions: [],
    avatar: 0,
    rechargeAmounts: [],
    schoolLevelName: "",
    levelGradeName: "",
    parentId: 0,
    schoolId: 0,
    schoolLevelId: 0,
    levelGradeId: 0,
    parent: {
      id: 0,
      firstName: "",
      lastName: "",
      email: "",
      typeDocument: "",
      documentNumber: "",
      phone: "",
      tokenNotification: "",
      userSub: "",
      userId: 0,
      createdAt: "",
      updatedAt: "",
      deletedAt: null,
    },
    schoolLevel: {
      id: 0,
      name: "",
      createdAt: "",
      updatedAt: "",
      deletedAt: null,
    },
    levelGrade: {
      id: 0,
      name: "",
      order: 0,
      schoolId: 0,
      schoolLevelId: 0,
      createdAt: "",
      updatedAt: "",
      deletedAt: null,
    },
    id: 0,
    balanceAmount: 0,
    createdAt: "",
    updatedAt: "",
    deletedAt: null,
  },
  loadDataChange: false
}

const _ChildReducer = createReducer(
  initialState,

  on(ChildActions.setChild, (state, { child }) => ({
    ...state,
    child: child
  })),

  on(ChildActions.setListChild, (state, { childList }) => ({
    ...state,
    childList: [...childList]
  })),

  on(ChildActions.addChild, (state, { child }) => ({
    ...state,
    childList: [...state.childList, { ...child }],
  })),

  on(ChildActions.updateChild, (state, { updatedChild }) => ({
    ...state,
    childList: state.childList.map(child =>
      child.id === updatedChild.id ? { ...child, ...updatedChild } : child
    ),
  })),

  on(ChildActions.removeChild, (state, { childId }) => ({
    ...state,
    childList: state.childList.filter(child => child.id !== childId),
  }))
);

export function ChildReducer(state: any, action: Action) {
  return _ChildReducer(state, action);
}
