import { Action, createReducer, on } from '@ngrx/store';
import { ServiceActions } from '../actions/service.actions';
import { Service } from '../../config/interfaces/service.interface';

export interface ServiceState {
  serviceList: Service[];
  notification: number;
  loadData: boolean;
}

export const initialState: ServiceState = {
  serviceList: [],
  notification: 0,
  loadData: false
}

const _ServiceReducer = createReducer(
  initialState,

  on(ServiceActions.setServiceList, (state, { serviceList }) => ({
    ...state,
    serviceList: [...serviceList],
    loadData: true,
  })),

  on(ServiceActions.setNotification, (state, { notification }) => ({
    ...state,
    notification: state.notification + 1,
  })),

  on(ServiceActions.clearNotification, (state, { notification }) => ({
    ...state,
    notification: 0,
  })),
);

export function ServiceReducer(state: any, action: Action) {
  return _ServiceReducer(state, action);
}
