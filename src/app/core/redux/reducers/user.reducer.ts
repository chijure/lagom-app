import { Action, createReducer, on } from '@ngrx/store';
import * as UserActions from '../actions/user.actions';
import { Profile } from '../../config/interfaces/app.interfaces';
import { Student } from '../../config/interfaces/child.interfaces';

export interface UserState {
  profile: Profile;
  child: Student;
  loadData: boolean;
}

export const initialState: UserState = {
  profile: {
    id: 0,
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    documentNumber: "",
    userId: 0,
  },
  child: {
    firstName: "",
    lastName: "",
    typeDocument: "",
    documentNumber: "",
    studentCode: "",
    medicalConditions: [],
    avatar: 0,
    rechargeAmounts: [],
    schoolLevelName: "",
    levelGradeName: "",
    parentId: 0,
    schoolId: 0,
    schoolLevelId: 0,
    levelGradeId: 0,
    parent: {
      id: 0,
      firstName: "",
      lastName: "",
      email: "",
      typeDocument: "",
      documentNumber: "",
      phone: "",
      tokenNotification: "",
      userSub: "",
      userId: 0,
      createdAt: "",
      updatedAt: "",
      deletedAt: null,
    },
    schoolLevel: {
      id: 0,
      name: "",
      createdAt: "",
      updatedAt: "",
      deletedAt: null,
    },
    levelGrade: {
      id: 0,
      name: "",
      order: 0,
      schoolId: 0,
      schoolLevelId: 0,
      createdAt: "",
      updatedAt: "",
      deletedAt: null,
    },
    id: 0,
    balanceAmount: 0,
    createdAt: "",
    updatedAt: "",
    deletedAt: null,
  },
  loadData: false
}

const _UserReducer = createReducer(
  initialState,

  on(UserActions.setProfile, (state, { profile }) => (
    {
      ...state,
      profile: { ...profile },
      loadData: true,
    })),

  on(UserActions.onLoadProfile, (state, { profile }) => {
    return { ...state, profile };
  }),
);

export function UserReducer(state: any, action: Action) {
  return _UserReducer(state, action);
}
