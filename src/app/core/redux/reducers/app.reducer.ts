import { Action, createReducer, on } from '@ngrx/store';
import * as AppDefaultActions from '../actions/app.actions';

export interface AppDefaultState {
  schoolList: any[];
  rolesList: any[];
  schoolLevelList: any[];
  serviceList: any[];
  categoryCoffeSchoolList: any[];
}

export const initialState: AppDefaultState = {
  schoolList: [],
  rolesList: [],
  schoolLevelList: [],
  serviceList: [],
  categoryCoffeSchoolList: [],
}

const _AppDefaultReducer = createReducer(
  initialState,

  on(AppDefaultActions.setSchoolList, (state, { schoolList }) => ({
    ...state,
    schoolList: schoolList
  })),

  on(AppDefaultActions.addSchoolList, (state, { school }) => ({
    ...state,
    schoolList: [...state.schoolList, school]
  })),

  on(AppDefaultActions.updateSchoolList, (state, { updatedSchool }) => {
    const updatedList = state.schoolList.map(school => {
      return school.id === updatedSchool.id ? updatedSchool : school;
    });

    return {
      ...state,
      schoolList: updatedList
    };
  }),

  on(AppDefaultActions.deleteSchoolList, (state, { schoolId }) => {
    const updatedList = state.schoolList.filter(school => school.id !== schoolId);

    return {
      ...state,
      schoolList: updatedList
    };
  }),

  on(AppDefaultActions.setRolesList, (state, { rolesList }) => ({
    ...state,
    rolesList: rolesList
  })),

  on(AppDefaultActions.setSchoolLevelList, (state, { schoolLevelList }) => ({
    ...state,
    schoolLevelList: schoolLevelList
  })),

  on(AppDefaultActions.setServiceList, (state, { serviceList }) => ({
    ...state,
    serviceList: serviceList
  })),

  on(AppDefaultActions.setCategoryCoffeSchoolList, (state, { categoryCoffeSchoolList }) => ({
    ...state,
    categoryCoffeSchoolList: categoryCoffeSchoolList
  }))
);

export function AppDefaultReducer(state: any, action: Action) {
  return _AppDefaultReducer(state, action);
}
