import { createAction, props } from '@ngrx/store';

/**
 * @description Acción para almacenar la data perfil
 */

export const setSchoolList = createAction(
  '[APPDEFAULT] setSchoolList',
  props<{ schoolList: any[] }>()
);

export const addSchoolList = createAction(
  '[APPDEFAULT] addSchoolList',
  props<{ school: any }>()
);

export const updateSchoolList = createAction(
  '[APPDEFAULT] updateSchoolList',
  props<{ updatedSchool: any }>()
);

export const deleteSchoolList = createAction(
  '[APPDEFAULT] deleteSchoolList',
  props<{ schoolId: number }>()
);

export const setRolesList = createAction(
  '[APPDEFAULT] setRolesList',
  props<{ rolesList: any[] }>()
);

export const setSchoolLevelList = createAction(
  '[APPDEFAULT] setSchoolLevelList',
  props<{ schoolLevelList: any[] }>()
);

export const setServiceList = createAction(
  '[APPDEFAULT] setServiceList',
  props<{ serviceList: any[] }>()
);

export const setCategoryCoffeSchoolList = createAction(
  '[APPDEFAULT] setCategoryCoffeSchoolList',
  props<{ categoryCoffeSchoolList: any[] }>()
);

export const AppActions = {
  setSchoolList,
  addSchoolList,
  updateSchoolList,
  deleteSchoolList,
  setRolesList,
  setSchoolLevelList,
  setServiceList,
  setCategoryCoffeSchoolList,
}
