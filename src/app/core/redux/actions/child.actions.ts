import { createAction, props } from '@ngrx/store';
import { Profile } from '../../config/interfaces/app.interfaces';
import { Student } from '../../config/interfaces/child.interfaces';

/**
 * @description Acción para almacenar la data perfil
 */

export const setChild = createAction(
  '[CHILD] setChild',
  props<{ child: Student }>()
);

export const addChild = createAction(
  '[CHILD] Add Child',
  props<{ child: Student }>()
);

export const setListChild = createAction('[CHILD] Set List of Children', props<{ childList: Student[] }>());

export const updateChild = createAction(
  '[CHILD] Update Child',
  props<{ updatedChild: Student }>()
);

export const removeChild = createAction(
  '[CHILD] Remove Child',
  props<{ childId: number }>()
);

export const ChildActions = {
  setChild,
  addChild,
  setListChild,
  updateChild,
  removeChild,
}
