import { createAction, props } from '@ngrx/store';
import { Profile } from '../../config/interfaces/app.interfaces';
import { Student } from '../../config/interfaces/child.interfaces';

/**
 * @description Acción para almacenar la data perfil
 */

export const setProfile = createAction(
  '[PROFILE] setProfile',
  props<{ profile: Profile }>()
);

export const onLoadProfile = createAction(
  '[PROFILE] onLoadProfile',
  props<{ profile: Profile }>()
);

export const UserActions = {
  setProfile,
  onLoadProfile,
}
