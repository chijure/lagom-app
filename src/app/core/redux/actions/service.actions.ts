import { createAction, props } from '@ngrx/store';
import { Service } from '../../config/interfaces/service.interface';

/**
 * @description Acción para almacenar la data perfil
 */

export const setServiceList = createAction(
  '[CHILD] Set List of Service',
  props<{ serviceList: Service[] }>()
);

export const setNotification = createAction(
  '[NOTIFICATION] setNotification',
  props<{ notification: number }>()
);

export const clearNotification = createAction(
  '[NOTIFICATION] clearNotification',
  props<{ notification: number }>()
);

export const ServiceActions = {
  setServiceList,
  setNotification,
  clearNotification
};
