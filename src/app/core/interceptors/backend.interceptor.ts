import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TOKEN } from '../config/constants/app.constants';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';

/**
 * When the HttpClient is used to make requests they are pointed to the same domain that
 * provides the angular app, this scenario only valid when the Angular app is served from the
 * same domain as the backend api but our architecture is different, we have separated domains
 * for the Angular app (manage.vetpraxis.cloud) and the backend api (api.vetpraxis.cloud).
 *
 * The BackendInterceptor modifies the http requests to point to the backend server set
 * in the `backendUrl` of the environment object when the request starts with `api/`,
 * `oauth/` or `config/`.
 * */
@Injectable()
export class BackendInterceptor implements HttpInterceptor {

  private authorization: any;

  constructor(private store: Store<AppState>){

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.authorization = localStorage.getItem(TOKEN);
    if (this.authorization) {
      req = req.clone({
        setHeaders: {
          ['Authorization']: this.authorization,
        },
      });
    }

    return next.handle(req);
  }
}
