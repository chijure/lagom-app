import { ChangeDetectorRef, Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { ToastService } from 'src/app/shared/services/toast/toast.service';
import { ModalService } from '../../shared/services/modal/modal.service';
import { NavController } from '@ionic/angular';

/**
 * This is Interceptor catch http errors to redirect on its general error pages
 */
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {


  constructor(private modalService: ModalService, private navController: NavController) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        tap((success) => {
          if (success instanceof HttpResponse && success.status === 201) {
            //console.log('success',success);
            if(success.body.status !== 201){
              //console.log(success.body.status);
            }
          }
        }),
        catchError((error: HttpErrorResponse) => {

          return throwError(error);
        }),
        finalize(() => {
        })
      );
  }
}
