import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class NotificationMessageService {
  private apiUrl = environment.api;

  constructor(private http: HttpClient) { }

  getPaginate(page: number, limit: number, date: string, slug: string): Observable<any> {
    let params = new HttpParams()
      .set('page', page)
      .set('limit', limit)
      .set('sortBy', 'createdAt:DESC')
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.NOTIFICATION_MESSAGE_CLIENT}/paginated`, { params });
  }

}
