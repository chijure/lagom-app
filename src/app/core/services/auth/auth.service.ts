import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';
/* import { Login } from '../../interfaces/auth.interfaces';
import { jwtDecode } from "jwt-decode"; */
import { TOKEN } from '../../config/constants/app.constants';
import { Login } from '../../config/interfaces/app.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = environment.api;
  private slug:string = 'guest';

  constructor(private http: HttpClient) {}

  isLoggedIn(): boolean {
    return !!localStorage.getItem(TOKEN);
  }

  /* decodeJwt(token:string){
    return jwtDecode(token);
  }*/

  login(data: Login): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.LOGIN_AUTH}`, data);
  }

  setSlug(slug:string){
    this.slug = slug;
  }

  validateRole(allowedRoles: string[]) : boolean {
    if (this.slug) {
      return allowedRoles.includes(this.slug);
    }

    return false;
  }

  sendCode(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.AUTH_SEND_CLIENT}`, data);
  }

  resetCode(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.AUTH_RESET_CLIENT}`, data);
  }
}
