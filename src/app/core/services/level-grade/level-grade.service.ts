import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class LevelGradeService {
  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  getAll(schoolId: number): Observable<any> {
    let params = new HttpParams()
    .set('schoolId', schoolId);
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.LEVEL_GRADE_CLIENT}/`, { params });
  }
}
