import { TestBed } from '@angular/core/testing';

import { LevelGradeService } from './level-grade.service';

describe('LevelGradeService', () => {
  let service: LevelGradeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LevelGradeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
