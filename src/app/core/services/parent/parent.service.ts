import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';
import { Observable } from 'rxjs';
import { TOKEN } from '../../config/constants/app.constants';

@Injectable({
  providedIn: 'root'
})
export class ParentService {
  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  create(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.PARENT_CLIENT}`, data);
  }

  update(data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/${ENDPOINTS.PARENT_CLIENT}`, data);
  }

  me(): Observable<any> {
    /* const authorization = localStorage.getItem(TOKEN) || '';
    const headers = new HttpHeaders({ 'Authorization': authorization }); */
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.PARENT_CLIENT}/me`);
  }
}
