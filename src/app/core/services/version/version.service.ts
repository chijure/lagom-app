import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  getVersion(platform: string): Observable<any> {
    let params = new HttpParams()
      .set('app', platform);
    return this.http.get<any>(`${this.apiUrl}/client/version`, { params });
  }
}
