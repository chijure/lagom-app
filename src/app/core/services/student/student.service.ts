import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.STUDENT_CLIENT}/`);
  }

  create(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.STUDENT_CLIENT}`, data);
  }

  update(data: any, id:number): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/${ENDPOINTS.STUDENT_CLIENT}/${id}`, data);
  }

  detail(id:number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.STUDENT_CLIENT}/${id}`);
  }

  delete(id:number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/${ENDPOINTS.STUDENT_CLIENT}/${id}`);
  }
}
