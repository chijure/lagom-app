import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {
  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.SCHOOL_CLIENT}/`);
  }
}
