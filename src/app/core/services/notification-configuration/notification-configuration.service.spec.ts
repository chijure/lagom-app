import { TestBed } from '@angular/core/testing';

import { NotificationConfigurationService } from './notification-configuration.service';

describe('NotificationConfigurationService', () => {
  let service: NotificationConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotificationConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
