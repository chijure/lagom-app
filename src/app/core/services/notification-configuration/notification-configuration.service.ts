import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class NotificationConfigurationService {
  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.NOTIFICATION_CONFIGURATION_CLIENT}/`);
  }

  update(data: any): Observable<any> {
    return this.http.patch<any>(`${this.apiUrl}/${ENDPOINTS.NOTIFICATION_CONFIGURATION_CLIENT}/update-status`, data);
  }
}
