import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ENDPOINTS } from '../../config/constants/endpoints.constants';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private apiUrl = environment.api;

  constructor(private http: HttpClient) {}

  createRecharge(data: any): Observable<any> {
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.TRANSACTION_RECHARGE_CLIENT}`, data, { headers: headers });
  }

  createOrder(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/${ENDPOINTS.TRANSACTION_ORDER_CLIENT}`, data);
  }

  getPaginateHistory(page: number, limit: number, studentId: number, date: string, slug: string): Observable<any> {
    let params = new HttpParams()
      .set('page', page)
      .set('limit', limit)
      .set('sortBy', 'createdAt:DESC')
      .set('filter.studentId', studentId)
      .set('date', date)
      .set('slug', slug)
      .set('filter.status', 'dispatched');
    return this.http.get<any>(`${this.apiUrl}/client/transaction-detail/paginated`, { params });
  }

  getPaginateMovements(page: number, limit: number, studentId: number): Observable<any> {
    let params = new HttpParams()
      .set('page', page)
      .set('limit', limit)
      .set('sortBy', 'createdAt:DESC')
      .set('filter.studentId', studentId);
    return this.http.get<any>(`${this.apiUrl}/client/transaction/paginated`, { params });
  }

  validatePlan(studentId: number): Observable<any> {
    let params = new HttpParams()
      .set('studentId', studentId);
    return this.http.get<any>(`${this.apiUrl}/${ENDPOINTS.TRANSACTION_VALIDATE_PLAN_ADMIN}`, { params });
  }
}
