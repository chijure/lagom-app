export interface Login {
  email: string;
  password: string;
  tokenNotification: string;
}

export interface Profile {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  documentNumber: string;
  userId: number;
}
