interface SchoolLevelPrice {
  planPrice: number;
  schoolLevelId: number;
  regularPriceOD: number;
  regularPricePO: number;
}

interface ServiceConfiguration {
  id: number;
  isActive: boolean;
  schoolId: number;
  serviceId: number;
  schoolLevelId: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}

interface Image {
  urlS3: string;
  pathS3: string;
  schoolLevelId: string; // Asumiendo que schoolLevelId es una cadena (string)
}

interface ServicePlanning {
  id: number;
  interval: string;
  images: Image[];
  schoolId: number;
  serviceId: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}

export interface Service {
  id: number;
  name: string;
  displayName: string;
  description: string | null;
  price: number;
  schoolLevelPrices: SchoolLevelPrice[];
  generalServiceId: number;
  schoolId: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
  serviceConfigurations: ServiceConfiguration[];
  servicePlanning: ServicePlanning[];
}
