interface MedicalCondition {
  condition: string;
  treatment: string;
}

interface Parent {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  typeDocument: string;
  documentNumber: string;
  phone: string;
  tokenNotification: string;
  userSub: string;
  userId: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}

interface SchoolLevel {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}

interface LevelGrade {
  id: number;
  name: string;
  order: number;
  schoolId: number;
  schoolLevelId: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}

export interface Student {
  firstName: string;
  lastName: string;
  typeDocument: string;
  documentNumber: string;
  studentCode: string;
  medicalConditions: MedicalCondition[];
  avatar: number;
  rechargeAmounts: number[];
  schoolLevelName: string;
  levelGradeName: string;
  parentId: number;
  schoolId: number;
  schoolLevelId: number;
  levelGradeId: number;
  parent: Parent;
  schoolLevel: SchoolLevel;
  levelGrade: LevelGrade;
  id: number;
  balanceAmount: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
}
