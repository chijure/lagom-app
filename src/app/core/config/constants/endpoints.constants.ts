export const ENDPOINTS = {
  AUTH_SEND_CLIENT: 'client/auth/send-code-to-reset-credentials',
  AUTH_RESET_CLIENT: 'client/auth/reset-credentials',
  LOGIN_AUTH: 'client/auth/login',
  PARENT_CLIENT: 'client/parent',
  SCHOOL_CLIENT: 'client/school',
  LEVEL_GRADE_CLIENT: 'client/level-grade',
  STUDENT_CLIENT: 'client/student',
  NOTIFICATION_CONFIGURATION_CLIENT: 'client/notification-configuration',
  TRANSACTION_RECHARGE_CLIENT: 'client/transaction/create-recharge',
  TRANSACTION_ORDER_CLIENT: 'client/transaction/create-order',
  TRANSACTION_VALIDATE_PLAN_ADMIN: 'client/transaction/check-active-plan',
  NOTIFICATION_MESSAGE_CLIENT: 'client/notification-message',
  SERVICE_CLIENT: 'client/service',
}
