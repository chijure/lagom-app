export const PATHS = {
  DASHBOARD: 'dashboard',

  NOT_ALLOWED: 'no-permitido',

  LOGIN: '',
  LOGIN_FORM: 'inicio-sesion',
  FORGOT_PASSWORD: 'olvidaste-contrasena',
  CODE_PASSWORD: 'codigo-contrasena',
  FORM_PASSWORD: 'formulario-contrasena',
  REGISTER_PARENT: 'registro-padre',

  HOME_PARENT: 'inicio-padre',
  FORM_CHILD_PARENT: 'registrar-hijo',
  PROFILE_PARENT: 'perfil',
  NOTIFICATION_PARENT: 'notificaciones',
  CONFIGURATION_PARENT: 'configuracion',
  FAQ_PARENT: 'faq',

  HOME_CHILDREN: 'preorder',
  MENU_MONTHLY_CHILDREN: 'menu-mensual',
  MENU_DIARY_CHILDREN: 'menu-diario',
  BALANCE_CHILDREN: 'balance',
  BALANCE_RECHARGE_CHILDREN: 'recarga',
  CONSUMPTION_CHILDREN: 'consumo',
  PROFILE_CHILDREN: 'perfil',

  ROOT_PARENT: 'padre',
  ROOT_CHILDREN: 'hijo'
}

export type TypeColor = 'success' | 'error';
export type TypeHeader = 'back-title' | 'title' | 'menu-title' | 'menu-child' | 'back' | 'menu' | 'menu-logo' | 'menu-title-icon';
export type TypeFilterDate = 'days-range' | 'month-range' | 'week-range';

export const TOKEN = 'token';
export const REMEMBER_PASSWORD = 'remember_password';

export enum SERVICES {
  COFFEESHOPT = 1,
  MENU = 2,
}

export const MONTHS = [
  { id: '01', name: 'Enero' },
  { id: '02', name: 'Febrero' },
  { id: '03', name: 'Marzo' },
  { id: '04', name: 'Abril' },
  { id: '05', name: 'Mayo' },
  { id: '06', name: 'Junio' },
  { id: '07', name: 'Julio' },
  { id: '08', name: 'Agosto' },
  { id: '09', name: 'Septiembre' },
  { id: '10', name: 'Octubre' },
  { id: '11', name: 'Noviembre' },
  { id: '12', name: 'Diciembre' },
]

export const PAY_LIST = [
  { id: 1, name: 'Wallet' },
  { id: 2, name: 'No Wallet' },
]

export const IMAGES_AVATAR = [
  { id: 1, image: 'assets/image/svg/avatars/avatar-1.svg'},
  { id: 2, image: 'assets/image/svg/avatars/avatar-2.svg'},
  { id: 3, image: 'assets/image/svg/avatars/avatar-3.svg'},
  { id: 4, image: 'assets/image/svg/avatars/avatar-4.svg'},
  { id: 5, image: 'assets/image/svg/avatars/avatar-5.svg'},
  { id: 6, image: 'assets/image/svg/avatars/avatar-6.svg'},
  { id: 7, image: 'assets/image/svg/avatars/avatar-7.svg'},
  { id: 8, image: 'assets/image/svg/avatars/avatar-8.svg'},
  { id: 9, image: 'assets/image/svg/avatars/avatar-9.svg'},
];

export const CONTACTS = [
  {
    name: 'Contacto 01',
    cel: '957706302',
    email: 'info@lagom.pe',
  }
]

export const QUESTIONS = [
  {
    id: 1,
    question: '¿Cuánto debería depositar en la cuenta de mis hijos?',
    answer: 'En promedio, un estudiante gasta alrededor de S/ 300 al mes basándose en nuestras comidas planificadas semanalmente. Para aquellos que también consumen comidas diarias de la cafetería, el gasto promedio mensual aumenta a S/ 583. Sin embargo, esta cantidad puede variar según la edad y actividades del estudiante.',
  },
  {
    id: 2,
    question: '¿Se pueden realizar pagos con la aplicación bancaria?',
    answer: 'Actualmente la aplicación Lagom no admite funciones de pago a través de aplicaciones bancarias.',
  },
  {
    id: 3,
    question: '¿La aplicación muestra el consumo en tiempo real?',
    answer: 'Nuestro sistema actualiza la información de consumo diariamente. Si no está familiarizado con la aplicación, ¡no se preocupe! Estoy aquí para guiarlo en el proceso de acceso a la cuenta de su hijo. Solo díganos lo que necesita.',
  },
  {
    id: 4,
    question: 'Si no estoy familiarizado con la aplicación, ¿cómo puedo acceder a la cuenta de mi hijo?',
    answer: 'Independientemente de la aplicación, nuestro sistema enviará un correo electrónico con el saldo e información de consumo del estudiante mensualmente.',
  }
]
