import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AppReducer, AppState } from './app.reducer';
import { httpInterceptorProviders } from './core/interceptors';
import { localStorageSync } from 'ngrx-store-localstorage';


const metaReducers: MetaReducer<AppState>[] = [localStorageSyncReducer];

export function localStorageSyncReducer(reducer: any): any {
  return localStorageSync({ keys: ['user','appDefault'], rehydrate: true })(reducer);
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      backButtonText: 'Atrás',
      backButtonIcon: './assets/icon/svg/icon-back.svg',
      mode: 'md',
      innerHTMLTemplatesEnabled: true
    }),
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    StoreModule.forRoot( AppReducer, {
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      },
      metaReducers
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
      autoPause: true, // Pauses recording actions and state changes when the extension window is not open
    }),
  ],
  providers: [
    httpInterceptorProviders,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
