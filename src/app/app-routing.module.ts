import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PATHS } from './core/config/constants/app.constants';
import { AuthGuard } from './core/guards/auth/auth.guard';
import { ParentLayoutPage } from './layouts/parent-layout/parent-layout.page';
import { AuthLayoutPage } from './layouts/auth-layout/auth-layout.page';
import { ChildrenNavigationPageModule } from './layouts/children-navigation/children-navigation.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
  {
    path: PATHS.ROOT_PARENT,
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('src/app/layouts/parent-layout/parent-layout.module').then(
        (m) => m.ParentLayoutPageModule
      ),
  },
  {
    path: PATHS.ROOT_CHILDREN,
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('src/app/layouts/children-navigation/children-navigation.module').then(
        (m) => m.ChildrenNavigationPageModule
      ),
  },
  {
    path: '',
    component: AuthLayoutPage,
    loadChildren: () =>
      import('src/app/layouts/auth-layout/auth-layout.module').then(
        (m) => m.AuthLayoutPageModule
      ),
  },
  {
    path: '**',
    redirectTo: '',
  },
  {
    path: 'login-form',
    loadChildren: () => import('./pages/auth/login-form/login-form.module').then( m => m.LoginFormPageModule)
  },
  {
    path: 'code-password',
    loadChildren: () => import('./pages/auth/code-password/code-password.module').then( m => m.CodePasswordPageModule)
  },
  {
    path: 'form-password',
    loadChildren: () => import('./pages/auth/form-password/form-password.module').then( m => m.FormPasswordPageModule)
  },
  {
    path: 'form-child',
    loadChildren: () => import('./pages/parent/form-child/form-child.module').then( m => m.FormChildPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
