import { ActionReducerMap } from '@ngrx/store';
import { RAppDefault, RChild, RService, RUser } from './core/redux';

export interface AppState {
    user: RUser.UserState;
    appDefault: RAppDefault.AppDefaultState;
    child: RChild.ChildState;
    service: RService.ServiceState;
}

export const AppReducer: ActionReducerMap<AppState> = {
    user: RUser.UserReducer,
    appDefault: RAppDefault.AppDefaultReducer,
    child: RChild.ChildReducer,
    service: RService.ServiceReducer,
};
