import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { finalize } from 'rxjs';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { ModalService } from 'src/app/shared/services/modal/modal.service';

@Component({
  selector: 'app-form-password',
  templateUrl: './form-password.page.html',
  styleUrls: ['./form-password.page.scss'],
})
export class FormPasswordPage implements OnInit {
  routes = PATHS;
  hide: boolean = true;
  hideConfirm: boolean = true;
  public form!: FormGroup;
  public submitted: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private fb: FormBuilder,
    private navController: NavController,
    private modalService:ModalService,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      const email = String(params['email']);
      const code = String(params['code']);
      this.initForm(email, code);
    });

  }

  initForm(email:string, code:string) {
    this.form = this.fb.group(
      {
        code: [{ value: code, disabled: false }],
        email: [{ value: email, disabled: false }],
        password: [
          { value: null, disabled: false },
          Validators.compose([Validators.required, Validators.maxLength(15)]),
        ],
        confirmPassword: [
          { value: null, disabled: false },
          Validators.compose([Validators.required, Validators.maxLength(15)]),
        ],
      },
      { validators: this.passwordMatchValidator }
    );
  }

  get f() {
    return this.form.controls;
  }

  send() {
    this.form.markAllAsTouched();
    this.submitted = true;
    if (!this.form.valid) {
      return;
    }

    const form = this.form.value;
    delete form.confirmPassword;

    console.log(form);

    this.authService
      .resetCode(form)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: (resAuth: any) => {
          this.modalService.showInfo(
            'Contraseña actualizada',
            'Se actualizó exitosamente la contraseña',
            'OK',
            () => {
              this.navController.navigateRoot([`/`]);
            }
          );
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        },
      });
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const passwordControl = formGroup.get('password');
    const confirmPasswordControl = formGroup.get('confirmPassword');

    if (
      passwordControl &&
      confirmPasswordControl &&
      passwordControl.value !== confirmPasswordControl.value
    ) {
      confirmPasswordControl.setErrors({ passwordMismatch: true });
    } else {
      confirmPasswordControl!.setErrors(null);
    }
  }
}
