import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormPasswordPage } from './form-password.page';

describe('FormPasswordPage', () => {
  let component: FormPasswordPage;
  let fixture: ComponentFixture<FormPasswordPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(FormPasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
