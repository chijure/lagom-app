import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormPasswordPageRoutingModule } from './form-password-routing.module';

import { FormPasswordPage } from './form-password.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormPasswordPageRoutingModule,
    SharedModule
  ],
  declarations: [FormPasswordPage]
})
export class FormPasswordPageModule {}
