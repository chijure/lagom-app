import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { finalize } from 'rxjs';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { ModalService } from 'src/app/shared/services/modal/modal.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  routes = PATHS;
  public form!: FormGroup;
  public submitted: boolean = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private modalService: ModalService,
    private navController: NavController,
    private authService: AuthService,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      email: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.maxLength(100),
        ]),
      ],
    });
  }

  get f() {
    return this.form.controls;
  }

  send() {
    this.form.markAllAsTouched();
    this.submitted = true;
    if (!this.form.valid) {
      return;
    }

    console.log(this.form.value.email);

    this.authService
    .sendCode(this.form.value)
    .pipe(
      finalize(() => {
        //stop load
        this.submitted = false;
      })
    )
    .subscribe({
      next: (resAuth: any) => {
        this.modalService.showInfo(
          'Código de validación',
          'El código de validación ha sido enviado a tu correo.',
          'OK',
          () => {
            this.navController.navigateForward([`/${PATHS.CODE_PASSWORD}/${this.form.value.email}/${resAuth.code}`]);
          }
        );
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customErrorSendCode(error);
        //this.navController.navigateForward([`/${PATHS.CODE_PASSWORD}/${this.form.value.email}`]);
      },
    });

  }
}
