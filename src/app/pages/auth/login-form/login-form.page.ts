import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { finalize } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { PATHS, TOKEN } from 'src/app/core/config/constants/app.constants';
import { Profile } from 'src/app/core/config/interfaces/app.interfaces';
import { UserActions } from 'src/app/core/redux/actions/user.actions';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ParentService } from 'src/app/core/services/parent/parent.service';
import { FCM_TOKEN } from 'src/app/shared/services/fcm/fcm.service';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { ErrorService } from '../../../shared/services/error/error.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.page.html',
  styleUrls: ['./login-form.page.scss'],
})
export class LoginFormPage implements OnInit {
  routes = PATHS;
  hide: boolean = true;
  public form!: FormGroup;
  public submitted: boolean = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private navController: NavController,
    private authService: AuthService,
    private parentService: ParentService,
    private modalService: ModalService,
    private storage: StorageService,
    private store: Store<AppState>,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      email: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.maxLength(100),
        ]),
      ],
      password: [
        { value: null, disabled: false },
        Validators.compose([Validators.required, Validators.maxLength(15)]),
      ],
    });
  }

  get f() {
    return this.form.controls;
  }

  async login() {
    this.submitted = true;
    if (!this.form.valid) {
      this.errorService.errorForm(this.form);
      return;
    }
    const form = this.form.value;
    const savedToken =
      JSON.parse((await this.storage.getStorage(FCM_TOKEN))) ||
      'Tokendeprueba';

    console.log('savedToken', savedToken);

    form.tokenNotification = savedToken;

    this.authService
      .login(this.form.value)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: (resAuth: any) => {
          localStorage.setItem(TOKEN, resAuth.token);
          this.parentService.me().subscribe({
            next: (resProfile: Profile) => {
              this.store.dispatch(
                UserActions.setProfile({ profile: resProfile })
              );
              this.navController.navigateRoot([`${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`]);
            },
            error: (error: HttpErrorResponse) => {
              this.errorService.customError(error);
            },
          });
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        },
      });
  }

  forgot() {
    this.navController.navigateForward([`${PATHS.FORGOT_PASSWORD}`]);
  }
}
