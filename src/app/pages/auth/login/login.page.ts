import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { PATHS } from 'src/app/core/config/constants/app.constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  routes = PATHS;
  constructor(private router: Router, private navController: NavController,) { }

  ngOnInit() {
  }

  login(){
    this.navController.navigateForward([`${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`])
  }

  forgot(){
    this.navController.navigateForward([`${PATHS.FORGOT_PASSWORD}`])
  }
}
