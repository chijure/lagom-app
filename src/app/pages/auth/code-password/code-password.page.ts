import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInput, NavController } from '@ionic/angular';
import { finalize } from 'rxjs';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { ModalService } from 'src/app/shared/services/modal/modal.service';

@Component({
  selector: 'app-code-password',
  templateUrl: './code-password.page.html',
  styleUrls: ['./code-password.page.scss'],
})
export class CodePasswordPage implements OnInit {
  @ViewChild('code1', { static: true }) myInput1: IonInput | undefined;
  @ViewChild('code2', { static: true }) myInput2: IonInput | undefined;
  @ViewChild('code3', { static: true }) myInput3: IonInput | undefined;
  @ViewChild('code4', { static: true }) myInput4: IonInput | undefined;

  routes = PATHS;
  public submitted: boolean = false;
  public form!: FormGroup;
  public email:string = '';
  private code: string = '';
  private regex = /[^0-9a-zA-Z]/g;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private navController: NavController,
    private authService: AuthService,
    private modalService: ModalService,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.email = String(params['email']);
      this.code = String(params['code']);
    });

    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      code1: [
        { value: null, disabled: false },
        Validators.compose([Validators.required, Validators.max(9)]),
      ],
      code2: [
        { value: null, disabled: false },
        Validators.compose([Validators.required, Validators.max(9)]),
      ],
      code3: [
        { value: null, disabled: false },
        Validators.compose([Validators.required, Validators.max(9)]),
      ],
      code4: [
        { value: null, disabled: false },
        Validators.compose([Validators.required, Validators.max(9)]),
      ],
    });
  }

  get f() {
    return this.form.controls;
  }

  send() {
    this.authService
    .sendCode({email: this.email})
    .pipe(
      finalize(() => {
        //stop load
      })
    )
    .subscribe({
      next: (resSend: any) => {
        this.code = resSend.code;
        this.modalService.showInfo(
          'Código de validación',
          'El código de validación ha sido enviado a tu correo.',
          'OK',
          () => {
          }
        );
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      },
    });
  }

  confirm() {
    this.form.markAllAsTouched();
    this.submitted = true;
    if (!this.form.valid) {
      return;
    }
    console.log(this.form.value);
    const code = Object.values(this.form.value).join('');
    if(code === this.code){
      this.modalService.showInfo('Recuperación de contraseña', 'El código es válido', 'Continuar', () => {
        this.navController.navigateForward([`/${PATHS.FORM_PASSWORD}/${this.email}/${code}`]);
      })
    } else {
      this.modalService.showInfo('Recuperación de contraseña', 'El código no es válido', 'Seguir intentando', () => {
      })
    }

  }

  upNumber1(inputCode: any) {
    inputCode.value = inputCode.value.replace(this.regex, '').trim();
    inputCode.value.length ? this.myInput2!.setFocus() : null;
  }
  upNumber2(inputCode: any) {
    inputCode.value = inputCode.value.replace(this.regex, '').trim();
    inputCode.value.length ? this.myInput3!.setFocus() : null;
  }
  upNumber3(inputCode: any) {
    inputCode.value = inputCode.value.replace(this.regex, '').trim();
    inputCode.value.length ? this.myInput4!.setFocus() : null;
  }
  next(inputCode: any) {
    inputCode.value = inputCode.value.replace(this.regex, '').trim();
    if (inputCode.value.length) {
      //this.codePhone(inputCode.value)
    }
  }
}
