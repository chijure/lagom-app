import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CodePasswordPage } from './code-password.page';

const routes: Routes = [
  {
    path: '',
    component: CodePasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CodePasswordPageRoutingModule {}
