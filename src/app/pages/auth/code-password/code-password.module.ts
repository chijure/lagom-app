import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CodePasswordPageRoutingModule } from './code-password-routing.module';

import { CodePasswordPage } from './code-password.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CodePasswordPageRoutingModule,
    SharedModule
  ],
  declarations: [CodePasswordPage]
})
export class CodePasswordPageModule {}
