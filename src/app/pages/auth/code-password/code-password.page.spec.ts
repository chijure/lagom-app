import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { CodePasswordPage } from './code-password.page';

describe('CodePasswordPage', () => {
  let component: CodePasswordPage;
  let fixture: ComponentFixture<CodePasswordPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CodePasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
