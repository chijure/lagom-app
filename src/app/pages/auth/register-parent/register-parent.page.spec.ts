import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RegisterParentPage } from './register-parent.page';

describe('RegisterParentPage', () => {
  let component: RegisterParentPage;
  let fixture: ComponentFixture<RegisterParentPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(RegisterParentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
