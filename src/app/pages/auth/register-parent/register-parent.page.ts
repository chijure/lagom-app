import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { finalize } from 'rxjs';
import { PATHS, TOKEN } from 'src/app/core/config/constants/app.constants';
import { FCM_TOKEN } from 'src/app/shared/services/fcm/fcm.service';
import { ParentService } from 'src/app/core/services/parent/parent.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Login, Profile } from 'src/app/core/config/interfaces/app.interfaces';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { UserActions } from 'src/app/core/redux/actions/user.actions';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-register-parent',
  templateUrl: './register-parent.page.html',
  styleUrls: ['./register-parent.page.scss'],
})
export class RegisterParentPage implements OnInit {
  routes = PATHS;
  hide: boolean = true;
  hideConfirm: boolean = true;
  public form!: FormGroup;
  public submitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private navController: NavController,
    private modalService: ModalService,
    private parentService: ParentService,
    private authService: AuthService,
    private storage: StorageService,
    private store: Store<AppState>,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group(
      {
        firstName: [
          { value: null, disabled: false },
          Validators.compose([
            Validators.required,
            Validators.maxLength(100),
            Validators.minLength(2),
          ]),
        ],
        lastName: [
          { value: null, disabled: false },
          Validators.compose([
            Validators.required,
            Validators.maxLength(100),
            Validators.minLength(2),
          ]),
        ],
        phone: [
          { value: null, disabled: false },
          Validators.compose([
            Validators.maxLength(15),
            Validators.minLength(9),
          ]),
        ],
        typeDocument: [{ value: 'dni', disabled: false }],
        documentNumber: [
          { value: null, disabled: false },
          Validators.compose([
            Validators.maxLength(15),
            Validators.minLength(8),
          ]),
        ],
        email: [
          { value: null, disabled: false },
          Validators.compose([
            Validators.required,
            Validators.email,
            Validators.maxLength(100),
            Validators.minLength(8),
          ]),
        ],
        password: [
          { value: null, disabled: false },
          Validators.compose([
            Validators.required,
            Validators.maxLength(15),
            Validators.minLength(6),
          ]),
        ],
        confirmPassword: [
          { value: null, disabled: false },
          Validators.compose([Validators.required, Validators.maxLength(15)]),
        ],
      },
      { validators: this.passwordMatchValidator }
    );
  }

  get f() {
    return this.form.controls;
  }

  async send() {
    this.form.markAllAsTouched();
    this.submitted = true;
    if (!this.form.valid) {
      this.errorService.errorForm(this.form);
      return;
    }

    const form = this.form.value;
    const savedToken = JSON.parse(
      (await this.storage.getStorage(FCM_TOKEN))
    ) || "Tokendeprueba";

    form.tokenNotification = savedToken;
    form.userSub = "asdasd";

    if (form.documentNumber === '') {
      form.documentNumber = null;
    }

    if (form.phone === '') {
      form.phone = null;
    }

    this.parentService
      .create(form)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: (res: any) => {
          const sendAuth: Login = {email: form.email, password: form.password, tokenNotification: form.tokenNotification}

          this.authService.login(sendAuth).subscribe({
            next: (resAuth:any) => {
              localStorage.setItem(TOKEN, resAuth.token);
              this.parentService.me().subscribe({
                next: (resProfile:Profile) => {
                  console.log('resProfile');
                  this.store.dispatch(UserActions.setProfile({ profile: resProfile }));
                  this.modalService.showInfo(
                    'Tu cuenta ha sido activada',
                    'Gracias por confirmar tu cuenta. Ahora podrás ingresar a la aplicación LAGOM',
                    'OK',
                    () => {
                      this.navController.navigateRoot([
                        `/${PATHS.ROOT_PARENT}/${PATHS.FORM_CHILD_PARENT}/0/0`,
                      ]);
                    }
                  );
                },
                error: (error:HttpErrorResponse) => {
                  this.errorService.customError(error);
                }
              });

            },
            error: (error:HttpErrorResponse) => {
              this.errorService.customError(error);
            }
          });
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        },
      });
  }

  passwordMatchValidator(formGroup: FormGroup) {
    const passwordControl = formGroup.get('password');
    const confirmPasswordControl = formGroup.get('confirmPassword');

    if (
      passwordControl &&
      confirmPasswordControl &&
      passwordControl.value !== confirmPasswordControl.value
    ) {
      confirmPasswordControl.setErrors({ passwordMismatch: true });
    } else {
      confirmPasswordControl!.setErrors(null);
    }
  }
}
