import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsumptionPageRoutingModule } from './consumption-routing.module';

import { ConsumptionPage } from './consumption.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumptionPageRoutingModule,
    SharedModule
  ],
  declarations: [ConsumptionPage]
})
export class ConsumptionPageModule {}
