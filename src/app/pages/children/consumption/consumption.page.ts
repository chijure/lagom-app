import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import * as moment from 'moment';

import { finalize } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { TransactionService } from 'src/app/core/services/transaction/transaction.service';
import { DateSelectService } from 'src/app/shared/services/date-select/date-select.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-consumption',
  templateUrl: './consumption.page.html',
  styleUrls: ['./consumption.page.scss'],
})
export class ConsumptionPage implements OnInit {
  public historyList: any[] = [];
  public datesList: any[] = [];
  public routes = PATHS;
  public child: any;
  public optionSelected = { value: '', label: 'Hoy', slug: 'days-range' };
  public consumptionList: any[] = [];
  public eventInfiniteScroll: any = null;
  public paginate = {
    page: 1,
    limit: 10,
    current: 1,
    last: 1,
  };

  constructor(
    private store: Store<AppState>,
    private transactionService: TransactionService,
    public actionSheetController: ActionSheetController,
    private dateSelectService: DateSelectService,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.datesList = this.dateSelectService.getDates();
    console.log(this.datesList);
    this.optionSelected = this.datesList[0];
    this.store.select('child', 'child').subscribe({
      next: (res: any) => {
        this.child = res;
        this.initData(res.id, true);
      }
    });
  }

  initData(childId: number, init:boolean) {
    this.transactionService
    .getPaginateHistory(this.paginate.page, this.paginate.limit, childId, this.optionSelected.value, this.optionSelected.slug)
    .pipe(
      finalize(() => {
        //stop load
      })
    )
    .subscribe({
      next: (res: any) => {
        res.data = res.data.map((consumption: any) => {
          const date = moment(consumption.dispatchDate);
          const dateFormat = date.utc().format("DD MMM LT");
          return {
            ...consumption,
            date: dateFormat,
          }
        })
        if(init){
          this.consumptionList = res.data;
        } else {
          this.consumptionList = Array.from(new Set([...this.consumptionList, ...res.data]));
        }
        this.paginate.current = res.meta.currentPage;
        this.paginate.last = res.meta.totalPages;
        console.log(this.consumptionList);
        if (this.eventInfiniteScroll) {
          this.eventInfiniteScroll.target.complete();
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      },
    });
  }

  onIonInfinite($event:any){
    console.log($event);
    this.eventInfiniteScroll = event;
    this.paginate.page++;
    this.initData(this.child.id, false);
  }

  async presentActionSheet() {
    const buttons: any = this.datesList.map((date: any) => ({
      text: `${date.label}`,
      handler: () => {
        this.historyList = [];
        this.optionSelected = date;
        this.paginate.page = 1;
        this.initData(this.child.id, true);
      },
    }));

    buttons.push({
      text: 'Cancelar',
      role: 'cancel',
      handler: () => {
        console.log('Cancelar seleccionado');
      },
    },);

    const actionSheet = await this.actionSheetController.create({
      header: 'Seleccione una opción',
      buttons: buttons,
    });

    await actionSheet.present();
  }
}
