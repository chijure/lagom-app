import { Component, OnInit } from '@angular/core';
import { PATHS } from '../../../core/config/constants/app.constants';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { StudentService } from '../../../core/services/student/student.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { HttpErrorResponse } from '@angular/common/http';
import { Student } from 'src/app/core/config/interfaces/child.interfaces';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  routes = PATHS;
  public profile: any = {
    firstName: '',
    lastName: '',
    studentCode: '',
    typeDocument: '',
    documentNumber: '',
    levelGradeName: '',
    medicalConditions: [],
    avatar: 1,
  };

  constructor(
    private studentService: StudentService,
    private store: Store<AppState>,
    private navController: NavController,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.initData();
  }

  edit() {
    this.navController.navigateForward([
      `/${PATHS.ROOT_PARENT}/${PATHS.FORM_CHILD_PARENT}/1/1`,
    ]);
  }

  initData() {
    this.store.select('child', 'child').subscribe({
      next: (res: any) => {
        this.studentService.detail(res.id).subscribe({
          next: (student) => {
            this.profile = student;
          },
          error: (error: HttpErrorResponse) => {
            this.errorService.customError(error);
          }
        });
      },
    });
  }
}
