import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionSheetController, NavController, Platform } from '@ionic/angular';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { Camera, CameraDirection, CameraResultType, CameraSource } from '@capacitor/camera';
import { Capacitor } from '@capacitor/core';
import { NativeSettings, AndroidSettings, IOSSettings } from 'capacitor-native-settings';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { TransactionService } from 'src/app/core/services/transaction/transaction.service';
import { finalize } from 'rxjs';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { ObservableService } from 'src/app/shared/services/observable/observable.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.page.html',
  styleUrls: ['./recharge.page.scss'],
})
export class RechargePage implements OnInit {
  selectedAmount = '';
  listAmount: any[] = [];
  childList: any[] = [];
  public form!: FormGroup;
  public submitted: boolean = false;
  child: any = {
    avatar: 1
  };
  blobData: any;
  public nameImage: string = '';

  constructor(
    private observableService: ObservableService,
    private fb: FormBuilder,
    public actionSheetController: ActionSheetController,
    private modalService: ModalService,
    private platform: Platform,
    private navController: NavController,
    private store: Store<AppState>,
    private transactionService: TransactionService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.initForm();
    this.requestPermissions(() => { });
  }

  get f() {
    return this.form.controls;
  }

  initForm() {
    this.form = this.fb.group({
      amount: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.max(100000),
          Validators.min(1),
        ]),
      ],
      capturePhoto: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
        ]),]
    });

    this.f['amount'].disable();

    this.initData();
  }

  initData() {
    this.store.select('child').subscribe({
      next: (res: any) => {
        this.child = res.child;
        this.childList = res.childList;
        this.listAmount = this.child.rechargeAmounts;
        //this.selectedAmount = this.listAmount[0];
        console.log('res', res);
        //this.f['amount'].patchValue(this.selectedAmount);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      }
    });
  }

  selectedItem(item: any) {
    this.selectedAmount = item;
    if (item == 'Otros') {
      this.f['amount'].enable();
      this.f['amount'].setErrors({ required: true });
      this.form.markAllAsTouched();
    } else {
      this.f['amount'].patchValue(item);
      this.f['amount'].disable();
      this.f['amount'].setErrors(null);
      this.form.markAllAsTouched();
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Seleccione una opción',
      buttons: [
        {
          text: 'Subir desde cámara',
          handler: () => {
            console.log('Opción cámara seleccionada');
            this.requestPermissions(
              () => {
                this.camera();
              }
            );
          },
        },
        {
          text: 'Subir desde galería',
          handler: () => {
            this.requestPermissions(
              () => {
                this.gallery();
              }
            );
            console.log('Opción galería seleccionada');
          },
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar seleccionado');
          },
        },
      ],
    });

    await actionSheet.present();
  }

  confirm() {
    console.log('form', this.form.value);
    this.submitted = true;

    if (!this.form.valid) {
      this.errorService.errorForm(this.form);
      return;
    }

    const form = this.form.value;
    const amount = this.selectedAmount == 'Otros' ? form.amount : this.selectedAmount;
    const formData = new FormData();
    console.log('capturePhoto', form.capturePhoto);

    formData.append('amount', amount.toString());
    formData.append('studentId', this.child.id.toString());
    formData.append('rechargeImage', this.blobData, `${form.capturePhoto.webPath}.${form.capturePhoto.format}`);

    this.modalService.showConfirmation(
      'Confirmación de recarga',
      `Se está recargando el monto S/.${amount} en la cuenta de su hij@ ${this.child.firstName}`,
      'Confirmar',
      'No',
      () => {

        this.transactionService.createRecharge(formData).pipe(
          finalize(() => {
            //stop load
            this.submitted = false;
          })
        ).subscribe({
          next: async (res) => {
            //set amount new
            this.child.balanceAmount = res.studentBalance;
            const chil = this.childList.find((x: any) => x.id === this.child.id);
            if (chil) {
              chil.balanceAmount = res.studentBalance;
            }
            this.store.dispatch(ChildActions.updateChild({updatedChild: this.child}))
            this.store.dispatch(ChildActions.setListChild({childList: this.childList}))

            this.observableService.rechargeSubject$.next(true);

            //success
            this.modalService.showInfo(
              'Recarga exitosa',
              'Se ha realizado exitosamente la recarga a tu hij@. Se ha enviado un correo con el detalle de la recarga',
              'OK',
              () => {
                this.navController.navigateRoot([
                  `${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.BALANCE_CHILDREN}`,
                ]);
              }
            )
          },
          error: (error: HttpErrorResponse) => {
            this.errorService.customError(error);
          }
        })
      },
      () => {
        this.submitted = false;
      },
    )
  }

  async camera() {
    console.log('camera');
    try {
      const image = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera,
        quality: 30,
        allowEditing: false,
        saveToGallery: true,
        direction: CameraDirection.Rear,
        webUseInput: true
      });

      var imageUrl: any = image.webPath;
      this.blobData = await fetch(imageUrl).then((res) => res.blob());

      //const segments = imageUrl.split("/");
      //const fileName = segments[segments.length - 1];
      this.nameImage = `captura.${image.format}`;
      console.log('camera', this.nameImage);
      this.f['capturePhoto'].setValue(image);
    } catch (e) {
      console.log('error camera', e);
      this.f['capturePhoto'].setValue(null);
    }
  }

  async gallery() {
    try {
      const image = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        source: CameraSource.Photos,
        quality: 30,
        allowEditing: false,
        saveToGallery: true,
      });

      var imageUrl: any = image.webPath;
      this.blobData = await fetch(imageUrl).then((res) => res.blob());

      //const segments = imageUrl.split("/");
      //const fileName = segments[segments.length - 1];
      this.nameImage = `captura.${image.format}`;
      console.log('gallery', this.nameImage);
      this.f['capturePhoto'].setValue(image);
    } catch (e) {
      console.log('error gallery', e);
      this.f['capturePhoto'].setValue(null);
    }
  }

  async requestPermissions(ok: Function) {
    if (Capacitor.isNativePlatform()) {
      const permissions = await Camera.requestPermissions()
      if (permissions.camera == "denied" || permissions.photos == "denied") {
        this.modalService.showInfo(
          'Permiso necesario',
          'Por favor habilite el permiso para poder acceder a la camara o galería de su dispositivo, para que pueda subir su comprobante',
          'OK',
          () => {
            if (this.platform.is('ios')) {
              NativeSettings.openIOS({
                option: IOSSettings.App,
              });
            } else {
              NativeSettings.openAndroid({
                option: AndroidSettings.ApplicationDetails,
              });
            }
          }
        )
      } else {
        ok();
      }
    }
  }

  clearImage(){
    this.nameImage = "";
    this.f['capturePhoto'].setValue(null);
    this.form.markAllAsTouched();
  }
}
