import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ModalService } from '../../../shared/services/modal/modal.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { TransactionService } from 'src/app/core/services/transaction/transaction.service';
import { finalize } from 'rxjs';
import { ObservableService } from 'src/app/shared/services/observable/observable.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import * as moment from 'moment';
@Component({
  selector: 'app-balance',
  templateUrl: './balance.page.html',
  styleUrls: ['./balance.page.scss'],
})
export class BalancePage implements OnInit {
  routes = PATHS;
  child: any = {
    avatar: 1
  };
  public balanceList: any[] = [];
  public eventInfiniteScroll: any = null;
  public paginate = {
    page: 1,
    limit: 10,
    current: 1,
    last: 1,
  }

  constructor(
    private store: Store<AppState>,
    private transactionService: TransactionService,
    private observableService: ObservableService,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.observableService.rechargeSubject$.subscribe((res) => {
      if (res) {
        console.log('observableService', this.child);
        this.initData(this.child.id, true);
      }
    });
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.store.select('child', 'child').subscribe({
      next: (res: any) => {
        console.log('child', this.child);
        this.child = res;
        this.initData(res.id, true);
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      }
    });
  }

  initData(childId: number, init:boolean) {
    this.transactionService
    .getPaginateMovements(this.paginate.page, this.paginate.limit, childId)
    .pipe(
      finalize(() => {
        //stop load
      })
    )
    .subscribe({
      next: (res: any) => {
        res.data = res.data.map((balance: any) => {
          const date = moment(balance.createdAt);
          const dateFormat = date.utc().format("DD MMM LT");
          return {
            ...balance,
            date: dateFormat,
          }
        })
        if(init){
          this.balanceList = res.data;
        } else {
          this.balanceList = Array.from(new Set([...this.balanceList, ...res.data]));
        }
        this.paginate.current = res.meta.currentPage;
        this.paginate.last = res.meta.totalPages;
        console.log(this.balanceList);
        if (this.eventInfiniteScroll) {
          this.eventInfiniteScroll.target.complete();
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      },
    });
  }

  onIonInfinite($event:any){
    console.log($event);
    this.eventInfiniteScroll = event;
    this.paginate.page++;
    this.initData(this.child.id, false);
  }
}
