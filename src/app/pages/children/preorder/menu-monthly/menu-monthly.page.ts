import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IonDatetime } from '@ionic/angular';
import { IonModal, NavController } from '@ionic/angular/common';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import * as moment from 'moment-timezone';

import { TransactionService } from 'src/app/core/services/transaction/transaction.service';
import { combineLatest, finalize, map } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-menu-monthly',
  templateUrl: './menu-monthly.page.html',
  styleUrls: ['./menu-monthly.page.scss'],
})
export class MenuMonthlyPage implements OnInit {
  minDate: string = '';
  maxDate: string = '';
  public form!: FormGroup;
  public submitted: boolean = false;
  showCalendar = false;
  discount = 40;
  childList: any[] = [];
  child: any = {
    avatar: 1
  };
  studentId: number = 0;
  serviceSelected: any = {
    id: 0,
    name: '',
    price: 0,
    archives: [
      {
        name: '',
        url: '',
      },
    ],
  };

  constructor(
    private modalService: ModalService,
    private store: Store<AppState>,
    private transactionService: TransactionService,
    private fb: FormBuilder,
    private navController: NavController,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.initForm();
    this.getCurrentDate();
    this.getLastDayOfNextMonth();

    //change
    this.store.select('child', 'child').subscribe({
      next: (resStudent) => {
        this.studentId = resStudent.id;
        this.child = resStudent;
        this.store.select('service', 'serviceList').subscribe({
          next: (resServices) => {
            this.serviceSelected.id = resServices[1].id;
            this.serviceSelected.name = resServices[1].displayName;
            this.serviceSelected.schoolLevelId = Number(resStudent.schoolLevelId);

            const schoolLevelPrice = resServices[1].schoolLevelPrices.find(
              (prices: any) => prices.schoolLevelId === resStudent.schoolLevelId
            );
            this.serviceSelected.price = schoolLevelPrice?.planPrice;

            const archives = resServices[1].servicePlanning
              .map((entry: any) =>
                entry.images
                  .filter((image: any) => String(image.schoolLevelId) === String(resStudent.schoolLevelId))
                  .map((image: any) => ({
                    interval: this.formatInterval(entry.interval),
                    name: entry.id.toString(),
                    schoolLevelId: resStudent.schoolLevelId,
                    url: image.urlS3,
                  }))
              )
              .reduce((acc, val) => acc.concat(val), []);

            this.serviceSelected.archives = archives;

            this.discount = 100 - (schoolLevelPrice?.planPrice || 0) / ((schoolLevelPrice?.regularPricePO || 0) * 20) * 100;

            console.log('serviceSelected', this.serviceSelected);

          }
        })
      }
    })

    this.store.select('child', 'childList').subscribe({
      next: (resStudentList) => {
        this.childList = resStudentList;
      }
    })
  }

  get f() {
    return this.form.controls;
  }

  initForm() {
    this.form = this.fb.group({
      start: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.max(100000),
          Validators.min(1),
        ]),
      ],
      end: [
        { value: null, disabled: true },
        Validators.compose([
          Validators.required,
          Validators.max(100000),
          Validators.min(1),
        ]),
      ],
    });

    this.f['end'].disable();
    this.setDateInit();
  }

  confirm() {
    console.log(this.form.value);
    console.log(this.form.get('end')!.value);
    this.submitted = true;
    if (!this.form.valid) {
      return;
    }

    moment.locale('es');
    const formattedDate = moment(this.form.value.start).tz('America/Lima').format('YYYY-MM-DD');
    const formattedDateStart = moment(this.form.value.start).tz('America/Lima').format(
      'D [de] MMMM'
    );
    const formattedDateEnd = moment(this.form.get('end')!.value).tz('America/Lima').format(
      'D [de] MMMM'
    );

    const form = {
      dates: [formattedDate],
      planType: 'monthly', //monthly, daily
      studentId: this.studentId,
      serviceId: this.serviceSelected.id,
    };

    this.modalService.showConfirmation(
      'Confirmar compra',
      `Estas comprando un <strong>${this.serviceSelected.name} mensual</strong>, del <strong>${formattedDateStart}</strong> hasta el <strong>${formattedDateEnd}</strong>. <br><br> Por un costo total de: <br><strong>S/. ${this.serviceSelected.price}</strong>`,
      'Confirmar',
      'No',
      () => {
        this.transactionService
          .createOrder(form)
          .pipe(
            finalize(() => {
              //stop load
              this.submitted = false;
            })
          )
          .subscribe({
            next: async (res) => {
              //set amount new
              this.child.balanceAmount = res.studentBalance;
              const chil = this.childList.find((x: any) => x.id === this.child.id);
              if (chil) {
                chil.balanceAmount = res.studentBalance;
              }
              this.store.dispatch(ChildActions.updateChild({ updatedChild: this.child }))
              this.store.dispatch(ChildActions.setListChild({ childList: this.childList }))

              //success
              this.modalService.showInfo(
                'Compra exitosa',
                'Se ha realizado exitosamente tu compra. Se enviará en tu correo la evidencia de la compra.',
                'OK',
                () => {

                  this.navController.navigateRoot([
                    `${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}`,
                  ]);
                }
              );
            },
            error: (error: HttpErrorResponse) => {
              this.errorService.customError(error);
            }
          });
      },
      () => { }
    );
  }

  isWeekday = (dateString: string) => {
    const date = new Date(dateString);
    const utcDay = date.getUTCDay();
    return utcDay !== 0 && utcDay !== 6;
  };

  getCurrentDate() {
    const now = moment();
    const isoString = now.toISOString();
    const formattedIsoString = moment(isoString).tz('America/Lima').format();
    this.minDate = formattedIsoString;
  }

  getLastDayOfNextMonth() {
    const now = new Date();

    const nextMonth = now.getMonth() + 1;
    const nextYear = now.getFullYear() + (nextMonth > 11 ? 1 : 0);

    const lastDayOfNextMonth = new Date(nextYear, nextMonth, 0);

    if (nextMonth === 2 && this.isLeapYear(now.getFullYear())) {
      lastDayOfNextMonth.setDate(29);
    }

    const isoString = lastDayOfNextMonth.toISOString().slice(0, 10);

    this.maxDate = isoString;
  }

  isLeapYear(year: number): boolean {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  }

  setDateInit() {
    const now = moment();

    if (now.day() === 6) {
      now.add(2, 'days');
    } else if (now.day() === 0) {
      now.add(1, 'days');
    }

    const isoString = now.toISOString();
    const formattedIsoString = moment(isoString).tz('America/Lima').format();

    this.f['start'].patchValue(formattedIsoString);

    const end = moment(now).add(29, 'days');
    const endISOString = end.toISOString();
    const formattedEndIsoString = moment(endISOString).tz('America/Lima').format();
    this.f['end'].patchValue(formattedEndIsoString);
  }

  setDateFinal() {
    const now = moment(this.f['start'].value);

    if (now.day() === 6) {
      now.add(2, 'days');
    } else if (now.day() === 0) {
      now.add(1, 'days');
    }

    const end = moment(now).add(29, 'days');
    const endISOString = end.toISOString();
    const formattedEndIsoString = moment(endISOString).tz('America/Lima').format();
    this.f['end'].patchValue(formattedEndIsoString);
  }

  openCalendar() {
    this.showCalendar = true;
  }
  cancelCalendar() {
    this.showCalendar = false;
    this.setDateFinal();
  }
  formatInterval = (interval: any) => {
    moment.locale('es');
    return moment(interval, 'YYYY-MM').tz('America/Lima').format('MMMM');
  };
}
