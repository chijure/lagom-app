import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuMonthlyPage } from './menu-monthly.page';

describe('MenuMonthlyPage', () => {
  let component: MenuMonthlyPage;
  let fixture: ComponentFixture<MenuMonthlyPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MenuMonthlyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
