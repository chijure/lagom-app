import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuMonthlyPage } from './menu-monthly.page';

const routes: Routes = [
  {
    path: '',
    component: MenuMonthlyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuMonthlyPageRoutingModule {}
