import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuMonthlyPageRoutingModule } from './menu-monthly-routing.module';

import { MenuMonthlyPage } from './menu-monthly.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuMonthlyPageRoutingModule,
    SharedModule
  ],
  declarations: [MenuMonthlyPage]
})
export class MenuMonthlyPageModule {}
