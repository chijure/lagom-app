import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreorderPageRoutingModule } from './preorder-routing.module';

import { PreorderPage } from './preorder.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreorderPageRoutingModule,
    SharedModule
  ],
  declarations: [PreorderPage]
})
export class PreorderPageModule {}
