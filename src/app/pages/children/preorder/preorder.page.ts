import { Component, OnInit } from '@angular/core';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { Service } from '../../../core/config/interfaces/service.interface';
import { ServiceService } from 'src/app/core/services/service/service.service';
import { finalize } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { ServiceActions } from 'src/app/core/redux/actions/service.actions';
import { NavController } from '@ionic/angular';
import { TransactionService } from '../../../core/services/transaction/transaction.service';
import { ModalService } from '../../../shared/services/modal/modal.service';
import * as moment from 'moment-timezone';

import { ErrorService } from 'src/app/shared/services/error/error.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { ObservableService } from 'src/app/shared/services/observable/observable.service';

@Component({
  selector: 'app-preorder',
  templateUrl: './preorder.page.html',
  styleUrls: ['./preorder.page.scss'],
})
export class PreorderPage implements OnInit {
  routes = PATHS;
  public child: any;
  public loadEvent = false;
  public servicesList:any[] = [
    {
      slug: 'card-monthly',
      name: '',
      description: 'Asegura un mes completo con nuestro Plan Mensual. Ahorra tiempo y disfruta de descuentos exclusivos.',
      isActive: false,
      link: `${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}/${PATHS.MENU_MONTHLY_CHILDREN}`
    },
    {
      slug: 'card-diary',
      name: '',
      description: 'Planifica con facilidad y flexibilidad la alimentación diaria de tu hijo.',
      isActive: false,
      link: `${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}/${PATHS.MENU_DIARY_CHILDREN}`
    }
  ];

  constructor(
    private serviceService: ServiceService,
    private store: Store<AppState>,
    private navController: NavController,
    private transactionService: TransactionService,
    private modalService: ModalService,
    private errorService: ErrorService,
    private observableService: ObservableService
  ) {
    this.observableService.changeChildSubject$.subscribe((res:any) => {
      if (res) {
        //console.log('observableService', res);
      }
    });
  }

  ngOnInit() {
    console.log('entro ngOnInit');
    this.store.select('child', 'child').subscribe({
      next: (resChild) => {
        console.log('resChild preorder', resChild);
        this.child = resChild;
        this.store.select('service', 'serviceList').subscribe({
          next: (resServices) => {
            if (Array.isArray(resServices) && resServices.length === 0) {
              console.log('servicios preorder api');
              this.initData(resChild.schoolId, resChild.schoolLevelId);
            } else {
              console.log('servicios preorder storage');
              this.getServiceDetails('Plan Mensual', 'planPrice', resServices, resChild.schoolLevelId);
              this.getServiceDetails('Plan Diario', 'regularPricePO', resServices, resChild.schoolLevelId);
            }
          }
        })
      }
    })
  }

  initData(schoolId:number, schoolLevelId:number) {
    this.serviceService
      .getAll(schoolId)
      .pipe(
        finalize(() => {
          //stop load
        })
      )
      .subscribe({
        next: (res) => {
          //this.servicesList = res;
          console.log('schoolLevelId', schoolLevelId);
          this.getServiceDetails('Plan Mensual', 'planPrice', res, schoolLevelId);
          this.getServiceDetails('Plan Diario', 'regularPricePO', res, schoolLevelId);

          console.log('services', res);
          this.store.dispatch(ServiceActions.setServiceList({serviceList: res}))

        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }

  getServiceDetails = (type: string, planPriceKey: string, res:any, schoolLevelId:number) => {
    const serviceIndex = type === 'Plan Mensual' ? 0 : 1;
    const serviceConfig = res[1].serviceConfigurations.find((config: any) => config.schoolLevelId === schoolLevelId);
    const schoolLevelPrice = res[1].schoolLevelPrices.find((prices: any) => prices.schoolLevelId === schoolLevelId);

    this.servicesList[serviceIndex].name = res[1].displayName + ` - ${type}`;
    this.servicesList[serviceIndex].isActive = serviceConfig.isActive && schoolLevelPrice[planPriceKey] !== 0;
  };

  validatePlan(service:any){
    this.transactionService.validatePlan(this.child.id).subscribe({
      next: (res) => {
        if(res.hasPlan && service.slug === 'card-monthly'){
          const formattedDate = moment(res.nextDate).tz('America/Lima').format("DD/MM");
          this.modalService.showInfo(
            'Plan mensual vigente',
            `¡Ya cuentas con un plan mensual! Puedes renovar este plan a partir del ${formattedDate}.`,
            'OK',
            () => {

            }
          )
        } else if(res.hasPlan && service.slug === 'card-diary'){
          this.modalService.showConfirmation(
            'Confirmación',
            'Tu hijo ya cuenta con un plan mensual, ¿Estás seguro que deseas comprar un plan diario?',
            'Sí',
            'No',
            () => {
              this.navController.navigateRoot([service.link]);
            }
          )
        } else {
          this.navController.navigateRoot([service.link]);
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      }
    })
  }
}
