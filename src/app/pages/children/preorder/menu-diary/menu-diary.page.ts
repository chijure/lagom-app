import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import * as moment from 'moment-timezone';

import { combineLatest, finalize, map } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { TransactionService } from 'src/app/core/services/transaction/transaction.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { ModalService } from 'src/app/shared/services/modal/modal.service';

@Component({
  selector: 'app-menu-diary',
  templateUrl: './menu-diary.page.html',
  styleUrls: ['./menu-diary.page.scss'],
})
export class MenuDiaryPage implements OnInit {

  minDate: string = '';
  maxDate: string = '';
  public textHelper: string = 'Visualiza la programación que tenemos para tu hij@';
  public form!: FormGroup;
  public submitted: boolean = false;
  public total = 0;
  showCalendar = false;
  childList: any[] = [];
  child: any = {
    avatar: 1
  };
  studentId:number = 0;
  serviceSelected: any = {
    id: 0,
    name: '',
    price: 0,
    priceRegular: 0,
    archives: [
      {
        name: '',
        url: '',
      },
    ],
  };

  constructor(private errorService: ErrorService, private modalService: ModalService,private store: Store<AppState>,private transactionService: TransactionService, private fb: FormBuilder, private navController: NavController) { }

  ngOnInit() {
    this.initForm();
    this.getCurrentDate();
    this.getLastDayOfNextMonth();

    this.store.select('child', 'child').subscribe({
      next: (resStudent) => {
        this.studentId = resStudent.id;
        this.child = resStudent;
        this.store.select('service', 'serviceList').subscribe({
          next: (resServices) => {

            if (Array.isArray(resServices) && resServices.length !== 0) {
              this.serviceSelected.id = resServices[1].id;
              this.serviceSelected.name = resServices[1].displayName;

              const schoolLevelPrice = resServices[1].schoolLevelPrices.find(
                (prices: any) => prices.schoolLevelId === resStudent.schoolLevelId
              );
              this.serviceSelected.price = schoolLevelPrice?.planPrice;
              this.serviceSelected.priceRegular = schoolLevelPrice?.regularPricePO;

              const archives = resServices[1].servicePlanning
                .map((entry: any) =>
                  entry.images
                    .filter((image: any) => String(image.schoolLevelId) === String(resStudent.schoolLevelId))
                    .map((image: any) => ({
                      interval: this.formatInterval(entry.interval),
                      name: entry.id.toString(),
                      schoolLevelId: resStudent.schoolLevelId,
                      url: image.urlS3,
                    }))
                )
                .reduce((acc, val) => acc.concat(val), []);

              this.serviceSelected.archives = archives;
              this.total = 0;
              console.log(this.serviceSelected);
            }
          }
        });
      }
    })

    this.store.select('child', 'childList').subscribe({
      next: (resStudentList) => {
        this.childList = resStudentList;
      }
    })

  }

  setTotal(){
    this.total = this.f['dates'].value.length * this.serviceSelected.priceRegular;
  }

  get f() {
    return this.form.controls;
  }

  initForm() {
    this.form = this.fb.group({
      dates: [],
    });
  }

  confirm(){
    console.log(this.form.value);
    this.submitted = true;
    if (!this.form.valid) {
      return;
    }

    const form = this.form.value;
    const send = {
      dates: form.dates,
      planType: "daily", //monthly, daily
      studentId: this.studentId,
      serviceId: this.serviceSelected.id,
    };

    this.modalService.showConfirmation(
      'Confirmar compra',
      `Estas comprando en un <strong>${this.serviceSelected.name} Diario</strong>, por la cantidad de <br> <strong>${form.dates.length} dias</strong>. <br><br> Por un costo total de: <br><strong>S/. ${this.total}</strong>`,
      'Confirmar',
      'No',
      () => {

        this.transactionService.createOrder(send).pipe(
          finalize(() => {
            //stop load
            this.submitted = false;
          })
        ).subscribe({
          next: async (res) => {
            //set amount new
            this.child.balanceAmount = res.studentBalance;
            const chil = this.childList.find((x: any) => x.id === this.child.id);
            if (chil) {
              chil.balanceAmount = res.studentBalance;
            }
            this.store.dispatch(ChildActions.updateChild({ updatedChild: this.child }))
            this.store.dispatch(ChildActions.setListChild({ childList: this.childList }))

            //success
            this.modalService.showInfo(
              'Compra exitosa',
              'Se ha realizado exitosamente tu compra. Se enviará a tu correo la evidencia de la compra.',
              'OK',
              () => {
                this.navController.navigateRoot([`${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}`]);
              }
            )
          },
          error: (error: HttpErrorResponse) => {
            this.errorService.customError(error);
          }
        })

      },
      () => {},
    )
  }

  isWeekday = (dateString: string) => {
    const date = new Date(dateString);
    const utcDay = date.getUTCDay();
    return utcDay !== 0 && utcDay !== 6;
  };

  getCurrentDate() {
    const now = moment();
    const isoString = now.toISOString();
    const formattedIsoString = moment(isoString).tz('America/Lima').format();
    this.minDate = formattedIsoString;
  }

  getLastDayOfNextMonth() {
    const now = new Date();

    const nextMonth = now.getMonth() + 2;
    const nextYear = now.getFullYear() + (nextMonth > 11 ? 1 : 0);

    const lastDayOfNextMonth = new Date(nextYear, nextMonth, 0);

    if (nextMonth === 2 && this.isLeapYear(now.getFullYear())) {
      lastDayOfNextMonth.setDate(29);
    }

    const isoString = lastDayOfNextMonth.toISOString().slice(0, 10);
    const formattedIsoString = moment(isoString).tz('America/Lima').format();
    this.maxDate = formattedIsoString;
  }

  isLeapYear(year: number): boolean {
    return (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
  }

  formatInterval = (interval: any) => {
    moment.locale('es');
    return moment(interval, 'YYYY-MM').tz('America/Lima').format('MMMM');
  };
}
