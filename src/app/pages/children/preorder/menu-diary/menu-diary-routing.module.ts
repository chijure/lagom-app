import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuDiaryPage } from './menu-diary.page';

const routes: Routes = [
  {
    path: '',
    component: MenuDiaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuDiaryPageRoutingModule {}
