import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuDiaryPage } from './menu-diary.page';

describe('MenuDiaryPage', () => {
  let component: MenuDiaryPage;
  let fixture: ComponentFixture<MenuDiaryPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MenuDiaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
