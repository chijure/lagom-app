import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuDiaryPageRoutingModule } from './menu-diary-routing.module';

import { MenuDiaryPage } from './menu-diary.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuDiaryPageRoutingModule,
    SharedModule
  ],
  declarations: [MenuDiaryPage]
})
export class MenuDiaryPageModule {}
