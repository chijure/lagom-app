import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PreorderPage } from './preorder.page';

describe('PreorderPage', () => {
  let component: PreorderPage;
  let fixture: ComponentFixture<PreorderPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PreorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
