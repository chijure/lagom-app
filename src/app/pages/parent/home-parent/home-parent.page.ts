import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { finalize } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { ServiceActions } from 'src/app/core/redux/actions/service.actions';
import { StudentService } from 'src/app/core/services/student/student.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-home-parent',
  templateUrl: './home-parent.page.html',
  styleUrls: ['./home-parent.page.scss'],
})
export class HomeParentPage implements OnInit {

  studentList: any[] = [];
  constructor(private errorService: ErrorService, private studentService: StudentService, private navController: NavController, private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  ionViewWillEnter(){
    this.iniData();
  }

  iniData() {
    this.studentService.getAll().subscribe({
      next: (studentList) => {
        if (Array.isArray(studentList) && studentList.length) {
          this.studentList = studentList;
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      }
    });
  }

  async goToChildren(child:any) {
    this.store.dispatch(ServiceActions.setServiceList({serviceList: []}))
    this.store.dispatch(ChildActions.setListChild({childList: this.studentList}))
    this.store.dispatch(ChildActions.setChild({child}))
    this.navController.navigateRoot([`${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.HOME_CHILDREN}`])
  }

  add() {
    this.navController.navigateForward([`/${PATHS.ROOT_PARENT}/${PATHS.FORM_CHILD_PARENT}/1/0`])
  }
}
