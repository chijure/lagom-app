import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormChildPage } from './form-child.page';

const routes: Routes = [
  {
    path: '',
    component: FormChildPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormChildPageRoutingModule {}
