import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormChildPageRoutingModule } from './form-child-routing.module';

import { FormChildPage } from './form-child.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormChildPageRoutingModule,
    SharedModule
  ],
  declarations: [FormChildPage]
})
export class FormChildPageModule {}
