import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormChildPage } from './form-child.page';

describe('FormChildPage', () => {
  let component: FormChildPage;
  let fixture: ComponentFixture<FormChildPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(FormChildPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
