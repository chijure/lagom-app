import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IMAGES_AVATAR,
  PATHS,
  TypeHeader,
} from 'src/app/core/config/constants/app.constants';
import { IonicSlides, NavController } from '@ionic/angular';
import Swiper from 'swiper';
import { SchoolService } from 'src/app/core/services/school/school.service';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { LevelGradeService } from '../../../core/services/level-grade/level-grade.service';
import { StudentService } from 'src/app/core/services/student/student.service';
import { AppState } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { UserActions } from 'src/app/core/redux/actions/user.actions';
import { ChildActions } from 'src/app/core/redux/actions/child.actions';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-form-child',
  templateUrl: './form-child.page.html',
  styleUrls: ['./form-child.page.scss'],
})
export class FormChildPage implements OnInit {
  @ViewChild('swiper') swiperRef: ElementRef | undefined;
  swiper?: Swiper;

  routes = PATHS;
  hide: boolean = true;
  hideConfirm: boolean = true;
  typeHeader: TypeHeader = 'back-title';
  isEdit = 0;
  studentId = 0;
  public form!: FormGroup;
  swiperModules = [IonicSlides];
  public submitted: boolean = false;

  schoolList: any[] = [];
  levelGradesList: any[] = [];
  images = IMAGES_AVATAR;
  public child: any;

  constructor(
    private schoolService: SchoolService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private navController: NavController,
    private levelGradeService: LevelGradeService,
    private studentService: StudentService,
    private store: Store<AppState>,
    private modalService: ModalService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.initForm();
    this.initData();

    this.route.params.subscribe((params: any) => {
      this.typeHeader = Number(params['header']) === 0 ? 'title' : 'back-title';
      this.isEdit = Number(params['edit']) === 0 ? 0 : Number(params['edit']);
      if (this.isEdit !== 0) {
        //editar
        this.f['schoolId'].disable();
        this.store.select('child', 'child').subscribe({
          next: (res: any) => {
            this.child = res;
            this.studentId = res.id;
            this.studentService.detail(res.id).subscribe({
              next: async (student) => {
                const formattedMedicalConditions =
                  student.medicalConditions.map(
                    (conditionObject: any) => conditionObject.condition
                  );
                this.f['firstName'].patchValue(student.firstName);
                this.f['lastName'].patchValue(student.lastName);
                this.f['schoolId'].patchValue(student.schoolId);
                this.f['levelGradeId'].patchValue(student.levelGradeId);
                this.f['typeDocument'].patchValue(student.typeDocument);
                this.f['documentNumber'].patchValue(student.documentNumber);
                this.f['studentCode'].patchValue(student.studentCode);
                this.setMedicalConditionsControls(formattedMedicalConditions);
                this.getLevelGrades(student.schoolId);

                this.swiperRef?.nativeElement.swiper.slideTo(student.avatar - 1);
              },
              error: (error: HttpErrorResponse) => { },
            });
          },
        });
      } else {
        //crear
        this.f['levelGradeId'].disable();
      }
    });
  }

  ngAfterViewInit(): void { }

  get f() {
    return this.form.controls;
  }

  get listAllergies(): FormArray {
    return this.form.get('medicalConditions') as FormArray;
  }

  initForm() {
    this.form = this.fb.group({
      firstName: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.minLength(2),
        ]),
      ],
      lastName: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.minLength(2),
        ]),
      ],
      schoolId: [
        { value: '', disabled: false },
        Validators.compose([Validators.required]),
      ],
      levelGradeId: [
        { value: '', disabled: true },
        Validators.compose([Validators.required]),
      ],
      typeDocument: [{ value: 'dni', disabled: false }],
      documentNumber: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.maxLength(15),
          Validators.minLength(8),
        ]),
      ],
      studentCode: [
        { value: null, disabled: false },
        Validators.compose([Validators.maxLength(15), Validators.minLength(3)]),
      ],
      medicalConditions: this.fb.array([]),
    });

    this.listAllergies.push(this.fb.control('', [Validators.maxLength(100)]));
  }

  hasError(index: number, errorType: string): boolean {
    const control = this.listAllergies.at(index);
    return this.submitted && control.hasError(errorType);
  }

  add() {
    this.listAllergies.markAllAsTouched();
    if (this.listAllergies.valid) {
      this.listAllergies.push(
        this.fb.control('', [Validators.required, Validators.maxLength(100)])
      );
    }
  }

  remove(index: number) {
    this.listAllergies.removeAt(index);
  }

  send() {
    console.log(this.form.value);
    console.log(this.form.controls);
    this.form.markAllAsTouched();
    this.submitted = true;
    if (!this.form.valid) {
      this.errorService.errorForm(this.form);
      return;
    }

    const form = this.form.value;
    form.schoolId = this.f['schoolId'].value;
    form.avatar = this.swiperRef?.nativeElement.swiper.activeIndex + 1;
    const formattedMedicalConditions = form.medicalConditions.map(
      (condition: string) => {
        return {
          condition: condition,
          treatment: '',
        };
      }
    );
    form.medicalConditions = formattedMedicalConditions.filter(
      (x: any) => x.condition !== ''
    );
    console.log('form', form);

    if (form.documentNumber === '') {
      form.documentNumber = null;
    }

    if (this.isEdit === 0) {
      this.create(form);
    } else {
      this.update(form);
    }
  }

  create(form: any) {
    this.studentService
      .create(form)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: async (res: any) => {
          this.navController.navigateRoot([
            `${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`,
          ]);
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }

  update(form: any) {
    this.studentService
      .update(form, this.studentId)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: async (res: any) => {
          this.modalService.showInfo(
            'Datos actualizados',
            'Se actualizaron exitosamente los datos de tu hijo.',
            'OK',
            () => {
              this.store.dispatch(ChildActions.setChild({ child: res }));
              this.navController.navigateRoot([
                `${PATHS.ROOT_PARENT}/${PATHS.ROOT_CHILDREN}/${PATHS.PROFILE_CHILDREN}`,
              ]);
            }
          );
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }

  later() {
    this.navController.navigateRoot([
      `${PATHS.ROOT_PARENT}/${PATHS.HOME_PARENT}`,
    ]);
  }

  onSelectionChangeSchool(event: any): void {
    this.getLevelGrades(Number(this.f['schoolId'].value));
  }

  goNext() {
    this.swiperRef?.nativeElement.swiper.slideNext(1000);
  }

  goPrev() {
    this.swiperRef?.nativeElement.swiper.slidePrev(1000);
  }

  initData() {
    this.schoolService
      .getAll()
      .pipe(
        finalize(() => {
          //stop load
        })
      )
      .subscribe({
        next: (res: any) => {
          this.schoolList = res;
          console.log(this.schoolList);
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }

  getLevelGrades(idSchool: number) {
    this.levelGradeService
      .getAll(idSchool)
      .pipe(
        finalize(() => {
          //stop load
        })
      )
      .subscribe({
        next: (res: any) => {
          this.levelGradesList = res;
          this.f['levelGradeId'].enable();
          console.log(this.levelGradesList);
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }

  setMedicalConditionsControls(conditions: string[]) {
    // Clear existing controls
    while (this.listAllergies.length !== 0) {
      this.listAllergies.removeAt(0);
    }

    // Add controls for each condition
    conditions.forEach((condition: string) => {
      this.listAllergies.push(
        this.fb.control(condition, [
          Validators.required,
          Validators.maxLength(100),
        ])
      );
    });
  }
}
