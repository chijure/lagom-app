import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { NotificationConfigurationService } from '../../../../core/services/notification-configuration/notification-configuration.service';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { ModalService } from '../../../../shared/services/modal/modal.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {
  public form!: FormGroup;
  routes = PATHS;
  listChildren: any[] = [];
  submitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private notificationConfigurationService: NotificationConfigurationService,
    private modalService: ModalService,
    private errorService: ErrorService,
    private navController: NavController
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.notificationConfigurationService.getAll().subscribe({
      next: async (res: any) => {
        this.listChildren = res;

        if (this.listChildren.length !== 0) {
          const formControls: { [key: string]: any } = {};

          this.listChildren.forEach((child) => {
            child.notificationConfigurations.forEach((notification: any) => {
              formControls[notification.slug + '-' + notification.id] =
                this.fb.control(notification.isActive);
            });
          });

          this.form = this.fb.group(formControls);
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      }
    });
  }

  send() {
    const updatedNotifications = this.listChildren.map((child) => {
      return {
        id: child.id,
        notifications: child.notificationConfigurations.map(
          (notification: any) => ({
            id: notification.id,
            isActive:
              this.form.get(notification.slug + '-' + notification.id)?.value ||
              false,
          })
        ),
      };
    });

    this.submitted = true;

    this.notificationConfigurationService
      .update(updatedNotifications)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: (res: any) => {
          console.log('res', res);
          this.modalService.showInfo(
            'Cambios guardados',
            'Datos de notificaciones guardados exitosamente',
            'OK',
            () => {
              this.navController.navigateForward([
                `/${PATHS.ROOT_PARENT}/${PATHS.NOTIFICATION_PARENT}`,
              ]);
            }
          )
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }
}
