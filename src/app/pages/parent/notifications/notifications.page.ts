import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, NavController } from '@ionic/angular';
import * as moment from 'moment-timezone';
import { finalize } from 'rxjs';
import { CONTACTS, PATHS } from 'src/app/core/config/constants/app.constants';
import { NotificationMessageService } from 'src/app/core/services/notification-message/notification-message.service';
import { DateSelectService } from 'src/app/shared/services/date-select/date-select.service';
import { ModalService } from '../../../shared/services/modal/modal.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';
import { AppState } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { ServiceActions } from 'src/app/core/redux/actions/service.actions';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  public notificationsList: any[] = [];
  public datesList: any[] = [];
  public routes = PATHS;
  public contact = CONTACTS[0];
  public optionSelected = { value: '', label: 'Hoy', slug: 'days-range' };
  public eventInfiniteScroll: any = null;
  public paginate = {
    page: 1,
    limit: 10,
    current: 1,
    last: 1,
  }
  constructor(
    private notificationMessageService: NotificationMessageService,
    public actionSheetController: ActionSheetController,
    private navController: NavController,
    private dateSelectService: DateSelectService,
    private modalService: ModalService,
    private errorService: ErrorService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    console.log('entro notification');
    this.datesList = this.dateSelectService.getDatesNotifications();
    this.optionSelected = this.datesList[0];
    this.initData(true);
    this.store.dispatch(ServiceActions.clearNotification({notification: 0}))
    this.store.select('service', 'notification').subscribe({
      next: (res: any) => {
        console.log('rescargar notificaciones');
        this.paginate.page = 1;
        this.initData(true);
      },
    });
  }

  initData(init:boolean) {
    this.notificationMessageService
      .getPaginate(this.paginate.page, this.paginate.limit, this.optionSelected.value, this.optionSelected.slug)
      .pipe(
        finalize(() => {
          //stop load
        })
      )
      .subscribe({
        next: (res: any) => {
          console.log(res);
          res.data = res.data.map((notification: any) => {
            const date = moment(notification.createdAt);
            const dateFormat = date.tz('America/Lima').format("DD MMM LT");
            return {
              body: notification.body,
              date: dateFormat,
            }
          })
          if(init){
            this.notificationsList = res.data;
          } else {
            this.notificationsList = Array.from(new Set([...this.notificationsList, ...res.data]));
          }
          this.paginate.current = res.meta.currentPage;
          this.paginate.last = res.meta.totalPages;
          if (this.eventInfiniteScroll) {
            this.eventInfiniteScroll.target.complete();
          }
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }

  async presentActionSheet() {
    const buttons: any = this.datesList.map((date: any) => ({
      text: `${date.label}`,
      handler: () => {
        this.notificationsList = [];
        this.optionSelected = date;
        this.paginate.page = 1;
        this.initData(true);
      },
    }));

    buttons.push({
      text: 'Cancelar',
      role: 'cancel',
      handler: () => {
        console.log('Cancelar seleccionado');
      },
    },);

    const actionSheet = await this.actionSheetController.create({
      header: 'Seleccione una opción',
      buttons: buttons,
    });

    await actionSheet.present();
  }

  goToConfiguration() {
    this.navController.navigateForward([
      `/${PATHS.ROOT_PARENT}/${PATHS.CONFIGURATION_PARENT}`,
    ]);
  }

  confirmContact(){
    this.modalService.showConfirmationMessage(
      'Enviar error',
      '¿Quieres notificar algun error?',
      'Ir al correo',
      'En otro momento',
      () => {
        var feedback = document.createElement('a');
        feedback.setAttribute('href', 'mailto:' +this.contact.email);
        feedback.click();
      },
      () => {

      },
    )
  }

  onIonInfinite($event:any){
    console.log($event);
    this.eventInfiniteScroll = event;
    this.paginate.page++;
    this.initData(false);
  }

  handleRefresh(event:any){
    setTimeout(() => {
      this.paginate.page = 1;
      this.initData(true);
      console.log('initData handleRefresh');
      event.target.complete();
    }, 2000);
  }
}
