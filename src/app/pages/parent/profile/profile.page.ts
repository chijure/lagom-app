import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { StudentService } from 'src/app/core/services/student/student.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public profile: any;
  studentList: any[] = [];
  constructor(
    private studentService: StudentService,
    private store: Store<AppState>,
    private navController: NavController,
    private errorService: ErrorService
  ) {}

  ionViewWillEnter() {
    this.iniData();
  }

  ngOnInit() {
    this.store.select('user', 'profile').subscribe({
      next: (res: any) => {
        this.profile = res;
      },
    });
  }

  iniData() {
    this.studentService.getAll().subscribe({
      next: (studentList) => {
        if (Array.isArray(studentList) && studentList.length) {
          this.studentList = studentList;
        }
      },
      error: (error: HttpErrorResponse) => {
        this.errorService.customError(error);
      }
    });
  }

  edit() {
    this.navController.navigateForward([
      `/${PATHS.ROOT_PARENT}/${PATHS.PROFILE_PARENT}/1`,
    ]);
  }
}
