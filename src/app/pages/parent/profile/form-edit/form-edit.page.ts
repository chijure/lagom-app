import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { PATHS } from 'src/app/core/config/constants/app.constants';
import { ParentService } from '../../../../core/services/parent/parent.service';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { Profile } from 'src/app/core/config/interfaces/app.interfaces';
import { UserActions } from 'src/app/core/redux/actions/user.actions';
import { ModalService } from '../../../../shared/services/modal/modal.service';
import { ErrorService } from 'src/app/shared/services/error/error.service';

@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.page.html',
  styleUrls: ['./form-edit.page.scss'],
})
export class FormEditPage implements OnInit {
  routes = PATHS;
  hide: boolean = true;
  hideConfirm: boolean = true;
  public form!: FormGroup;
  public submitted: boolean = false;
  private profile:any;

  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder,
    private navController: NavController,
    private parentService: ParentService,
    private modalService: ModalService,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.initForm();
    this.store.select('user', 'profile').subscribe({
      next: (profile: any) => {
        this.profile = profile;
        this.form.setValue({
          firstName: profile.firstName,
          lastName: profile.lastName,
          phone: profile.phone,
          typeDocument: profile.typeDocument,
          documentNumber: profile.documentNumber,
          email: profile.email,
          password: null,
        });
      },
    });
  }

  initForm() {
    this.form = this.fb.group({
      firstName: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.minLength(2),
        ]),
      ],
      lastName: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          Validators.minLength(2),
        ]),
      ],
      phone: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.maxLength(15),
          Validators.minLength(9),
        ]),
      ],
      typeDocument: [{ value: 'dni', disabled: false }],
      documentNumber: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.maxLength(15),
          Validators.minLength(8),
        ]),
      ],
      email: [
        { value: null, disabled: false },
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.maxLength(100),
          Validators.minLength(8),
        ]),
      ],
      password: [
        { value: null, disabled: false },
        Validators.compose([
        ]),
      ],
    });
  }

  get f() {
    return this.form.controls;
  }

  send() {
    const form = this.form.value;
    console.log('form', this.form.value);
    console.log('form', this.form.controls);
    console.log('form', this.form.valid);

    this.form.markAllAsTouched();
    this.submitted = true;
    if (!this.form.valid) {
      this.errorService.errorForm(this.form);
      return;
    }

    console.log(!!form.password);

    if(!!form.email && !!form.password && form.email !== this.profile.email){
      this.modalService.showConfirmation(
        'Confirmación cambios',
        'Entre los cambios realizados, estás realizando un cambio de <strong>correo y contraseña</strong>, ¿estás seguro de esto?',
        'Confirmar',
        'No',
        () => {
          this.update(form);
        },
        () => {},
      );
    } else if(!!form.email && form.email !== this.profile.email){
      this.modalService.showConfirmation(
        'Confirmación cambios',
        'Entre los cambios realizados, estás realizando un cambio de <strong>correo</strong>, ¿estás seguro de esto?',
        'Confirmar',
        'No',
        () => {
          this.update(form);
        },
        () => {},
      );
    } else if(!!form.password){
      this.modalService.showConfirmation(
        'Confirmación cambios',
        '¿Entre los cambios realizados, estás realizando un cambio de <strong>contraseña</strong>, ¿estás seguro de esto?',
        'Confirmar',
        'No',
        () => {
          this.update(form);
        },
        () => {},
        );
      } else {
      this.update(form);
    }
  }

  update(form:any){
    if (form.documentNumber === '') {
      form.documentNumber = null;
    }

    if (form.phone === '') {
      form.phone = null;
    }

    this.parentService
      .update(form)
      .pipe(
        finalize(() => {
          //stop load
          this.submitted = false;
        })
      )
      .subscribe({
        next: (res: any) => {
          this.parentService.me().subscribe({
            next: (res: Profile) => {
              this.store.dispatch(UserActions.setProfile({ profile: res }));
              this.modalService.showInfo(
                'Cambios guardados',
                'Tus datos han sido actualizados exitosamente',
                'OK',
                () => {
                  this.navController.navigateRoot([
                    `/${PATHS.ROOT_PARENT}/${PATHS.PROFILE_PARENT}`,
                  ]);
                }
              )
            },
            error: (error: HttpErrorResponse) => {
              this.errorService.customError(error);
            }
          });
        },
        error: (error: HttpErrorResponse) => {
          this.errorService.customError(error);
        }
      });
  }
}
