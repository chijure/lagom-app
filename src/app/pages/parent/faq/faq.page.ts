import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { CONTACTS, QUESTIONS } from 'src/app/core/config/constants/app.constants';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  selectedSegment = 'questions';
  infoList: any[] = CONTACTS;
  questionsList: any[] = QUESTIONS;

  constructor(private platform: Platform) { }

  ngOnInit() {
  }
}
