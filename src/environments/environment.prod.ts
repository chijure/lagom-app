export const environment = {
  production: true,
  version: '0.0.1',
  api: 'https://colegios.lagom.pe/api'
};
